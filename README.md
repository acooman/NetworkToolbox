# NetworkToolbox

Collection of Matlab classes that allow easy representation of non-parametric Frequency response data and representations of electrical circuits.

## Classes

### FRM

Class that can hold non-parametric frequency response data for a system with an arbitrary number of inputs and outputs.

The two most important fields in an FRM are its frequency axis `Freq` and its frequency reponse data field called `Data` which contains a MultiMatrix.

The FRM saves the frequency axis in a normalised way.

Besides just storing the data in a convenient way, the FRM also has a `Domain` property which can be set to `PLANE` or `DISC` and allows to transform the FRM from one domain into the other.

### MultiPort

Class to represent the port-based representation of electrical circuits. 

A MultiPort is a square FRM which adds three important properties to an FRM: The S-parameters, the Z-parameters and the Y-parameters. They are available as dependent properties `S`, `Y` and `Z`. The reference impedance is saved in the `Ref` property.

The MultiPort is a sub-class of FRM, so it inherits all the functions available to an FRM as well.

### TwoPort

Class to represent two-port electrical circuits. 

Besides Y, Z and S-parameters, a two-port also has the following extra dependent propertied `G`, `H`, `A`, `B` and `T` parameters.

The TwoPort is a sub-class of MultiPort, so it inherits all the functions available to MultiPort and FRM as well.

### Class diagram

The collection of classes in the NetworkToolbox is the following:

<img src="http://yuml.me/diagram/class/[FRM|Freq;Theta;Freq_normalised;Domain|],[FRM]^-[MultiPort|Ref;Z;S;Y|],[MultiPort]^-[TwoPort|G;H;A;B;T|],[Transform|Forward();Backward();test()|≪F_par()≫ ≪B_par()≫;≪F_var()≫ ≪B_var()≫;≪F_fun()≫ ≪B_fun()≫;≪F_abcd()≫ ≪B_abcd()≫],[Transform]^-.-[MobiusTransform|alpha;theta_0],[Transform]^-[Normalisation|Fmin;Fmax|],[Normalisation]^-.-[LowpassNormalisation|],[Normalisation]^-.-[BandpassNormalisation|Fcenter;Fbandwidth],[FRM]<-[Transform],[FRM]<-[Normalisation],[FRM]<-[MultiMatrix||mtimes();times();...],[MultiMatrix]^-[builtin:double]"></img>

## Tools

The following usefull functions are also included in the repository:

**Interconnect**

Function that allows to interconnect a set of MultiPorts together to obtain the MultiPort of the total electrical system.


**Smithchart**

Function to plot S-parameter data on a smith chart. This function is way better that the function built-in function in the RF-toolbox of Matlab.


## Installation

To fully install the NetworkToolbox, perform the following steps:

1. Browse to the folder where you want the NetworkToolbox code to be saved

2. Run the following git commands to clone the code to your computer (Don't forge the --recursive at the end to clone all submodules as well)

```bash
git clone git@gitlab.inria.fr:acooman/NetworkToolbox.git --recursive
```

3. Finally, add the NetworkToolbox and folder and its sub-folders to the matlab path

## Requirements

Matlab 2016b or higher. Most of the code has been tested on Matlab 2017a

