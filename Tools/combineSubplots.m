function f = combineSubplots(Figs,siz1,siz2,index)
%% Number of subplots in each Figure
ax = cellfun(@(x) findobj(x,'type','axes'),Figs,'UniformOutput',false );
ax = cellfun(@(x) x(end:-1:1),ax,'UniformOutput',false );
pos = cellfun(@(x) (get(x,'position')),ax,'UniformOutput',false );
nrows = cellfun(@(x) numel(unique(x(:,1))),pos,'UniformOutput',true );
ncols = cellfun(@(x) numel(unique(x(:,2))),pos,'UniformOutput',true );

%% Now compute total number of figures
nfigs = nrows.*ncols;
N = sum(nfigs);

%% Check sizes
if nargin <= 3
    index = 1:N;
    if nargin <=2
        siz2 = [];
        if nargin == 1
            siz1 =[];
        end
    end
elseif numel(index)<N
    error('The indexing vector must contains as many elements as plots.');
end

%% Determine the format to concatenate
if isempty(siz1) && isempty(siz2)
    siz1 = ceil(sqrt(N));
    siz2 = siz1;
elseif isempty(siz1)
    siz1 = ceil(N./siz2);
elseif isempty(siz2)
    siz2 = ceil(N./siz1);
end

%% Move and resize figures
f = figure();
n = 1;
for i = 1:length(Figs)
    for k = 1:nfigs(i)
        s=subplot(siz1,siz2,index(n));
        p=get(s,'Position');
        delete(s);
        stemp = copyobj(ax{i}(k),f);
        set(stemp,'Position',p);
        n = n+1;
    end
    close(Figs{i});
end



