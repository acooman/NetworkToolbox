function is = is(x,c)

    is = cellfun(@(c) builtin('isa',x,c), c);

