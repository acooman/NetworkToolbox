function smithchart(varargin)
p=inputParser;
p.KeepUnmatched = true;
p.StructExpand = true;
defRaxis = [0 0.2 0.5  1 2 4 10];
defIaxis = [-5 -2 -1 -0.5 -0.2 0 0.2 0.5  1 2 5];
defResol = 100;
defColor = 0.87*[1 1 1];
defRlim  = 1;
p.addParameter('Realaxis',    defRaxis,     @isvector);
p.addParameter('Imagaxis',    defIaxis,     @isvector);
p.addParameter('Resolution',  defResol,     @isscalar);
p.addParameter('Color',       defColor,     @isvector);
p.addParameter('rlim',        defRlim,      @isscalar);
p.addParameter('polargrid',   false,        @islogical);
p.addParameter('annotation',   true,        @islogical);
p.addParameter('Type', 'Z', @(x) ismember(x,{'Z','Y'}));
p.parse(varargin{:});
args = p.Results;

switch args.Type
    case 'Z'
        flip = 1;
    case 'Y'
        flip = -1;
end

%% Plot a fake line to set the axis limit
% Polar plot does not allows to set the axis limits (the first polar plot 
% in a figure set them automaticaly and then they can not be changed.
hold off;
h = polar(0,abs(args.rlim));
set(h, 'Visible', 'off');
% We need to keep the hold on, otherwise the grid will reapear
hold on;

%% Remove lines and labels (this can not be done in a polar plot neither
if ~args.polargrid
delete(findall(gcf,'type','line'));
delete(findall(gcf,'type','text'));
end

%% Angle vector using resolution as number of points
ang=linspace(0,2*pi,args.Resolution).';

%% Deal with constant reactance circles first
ra = flip./(1+args.Realaxis);
ce = args.Realaxis.*ra;
PR = ce + ra.*exp(1i*ang);

%% Deal with constant impedance circles now
rc = 1./args.Imagaxis;
PI = flip +1i*rc + rc.*exp(1i*ang);

%% Real line
% if -1 belong to real axis, then plot vertical line at 1
if any(args.Realaxis == -1)
    vertline = 1+1i*linspace(-abs(args.rlim),abs(args.rlim),args.Resolution).';
    PR = [PR,vertline];
end

%% Real line
% if zero belong to imaginary axis, then plot real line
if any(args.Imagaxis == 0)
    realline = linspace(-abs(args.rlim),abs(args.rlim),args.Resolution).';
    PI = [PI,realline];
end

%% Polar plot
h = polar(angle([PR,PI]),abs([PR,PI]));
set(h,'Color',args.Color);
set(h,p.Unmatched);
% Group grid and exclude from legend
hGroup = hggroup;
set(h,'Parent',hGroup)
set(get(get(hGroup,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

if args.annotation
%% Some texting
% only labels inside the plot are shown
s = (args.Realaxis-1)./(1+args.Realaxis);
ind = abs(s)<=abs(args.rlim);
text(ce(ind)-ra(ind),zeros(1,length(args.Realaxis(ind))),num2str(args.Realaxis(ind).'));
% outer text position
z = 1i*args.Imagaxis -(abs(args.rlim)-0.75)./(abs(args.rlim)+1.25);
pos = (z-1)./(z+1);
text(real(pos),imag(pos),[num2str(args.Imagaxis.'),repmat('i',length(args.Imagaxis),1)]);
end





