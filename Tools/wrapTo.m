function angle_rad=wrapTo(angle_rad,wrap_value)

% angle_rad=wrapTo(angle_rad,wrap_value)
%           Wrap an angle to [wrap_value-2pi, wrap_value]
A = size(angle_rad);
W = size(wrap_value);
angle_rad = repmat(angle_rad, W);
wrap_value = repmat(wrap_value, A);

angle_rad = angle_rad + 2*pi*floor( (wrap_value-angle_rad)/(2*pi) ); 