function angle_rad=wrapTo2Pi(angle_rad)

% Wrap argument to [0, 2*pi]
angle_rad = angle_rad - 2*pi*floor(angle_rad/(2*pi));
