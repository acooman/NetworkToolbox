function angle_rad=wrapToPi(angle_rad)
% Wrap argument to [-pi, +pi]
angle_rad = wrapTo(angle_rad,pi);