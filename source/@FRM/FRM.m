classdef FRM
    % FRM   Value class that contains a network response
    %
    %   See also MULTIMATRIX, MULTIPORT.
    properties (Dependent)
        % This contains the data of the FRM. It is a MultiMatrix
        Data
        % Normalised frequency axis, between [-1 and 1]
        Freq_normalised
        % Frequency when the FRM is on the complex plane
        Freq
        % Angle when the FRM is on the unit disc
        Theta
        % normalisation object. Should be of the Normalisation class
        Normalisation
        % either 'PLANE' or 'DISC'. The transform function can be used to go from one domain to another
        Domain
        % Function handle to the function that allows to go from the complex plane to the unit disc and back
        Transform
    end
    properties (Access=protected)
        % Multimatrix containing the raw data
        data (:,:,:) MultiMatrix = MultiMatrix(0)
        % 1x1xF array which contains the frequency points at which the FRM is evaluated
        freq_normalised double
        % normalisation object, This implements the Normalisation class
        normalisation (1,1) Normalisation = BandpassNormalisation(0,1)
        % either 'PLANE' or 'DISC'
        domain (1,1) string = "PLANE"
        % Object which implements the Transform class
        transform (1,1) Transform = MobiusTransform
    end
    
    methods
        %% CONSTRUCTOR
        function obj = FRM(Data,varargin)
            % check if object is already FRM
            if isa(Data,'FRM')
                args = Data;
                obj.Data = args.Data;
            else
                % first fill in the data field
                obj.Data = MultiMatrix(Data);
                % create input parser
                p = inputParser;
                p.StructExpand = true;
                p.FunctionName = 'FRM';
                % when an FRM is being constructed, limit the parameters to
                % the ones defined here. When a MultiPort or TwoPort is
                % being constructed, we need to allow for unmatched
                % parameters here. With the risk of letting some parameters
                % pass through without them being set.
                if strcmpi(class(obj),'FRM')
                    p.KeepUnmatched = false;
                else
                    p.KeepUnmatched = true;
                end
                p.addParameter('Freq'    ,[],@obj.checkFreq);
                p.addParameter('Theta'   ,[],@obj.checkTheta);
                p.addParameter('Domain'  ,'PLANE',@obj.checkDomain);
                p.addParameter('Normalisation',[],@obj.checkNormalisation);
                p.addParameter('Transform',MobiusTransform,@obj.checkTransform);
                p.parse(varargin{:});
                args = p.Results;
            end
            % Expand data if necessary
            if length(obj.Data) == 1
                obj.Data = repmat(obj.Data,1,1,length(args.Freq));
            end
            % set the domain and the transform
            obj.domain   = upper(args.Domain);
            obj.transform = args.Transform;
            % set the frequency axis and the normalisation
            switch obj.domain
                case 'PLANE'
                    % first set the normalisation
                    if isempty(args.Normalisation)
                        obj.normalisation = BandpassNormalisation(args.Freq);
                    else
                        obj.normalisation = args.Normalisation;
                    end
                    % calling the dependent Freq property will normalise the frequency axis
                    if ~isempty(args.Freq)
                        obj.Freq = args.Freq;
                    else
                        error('When constructing an FRM on the plane, provide a frequency axis');
                    end
                case 'DISC'
                    % first set the normalisation.
                    if isempty(args.Normalisation)
                        obj.normalisation = BandpassNormalisation(-1,1);
                    else
                        obj.normalisation = args.Normalisation;
                    end
                    % assign the theta-axis
                    if ~isempty(args.Theta)
                        obj.Theta = args.Theta;
                    else
                        error('When constructing an FRM on the plane, provide a theta-axis');
                    end
            end
        end
        %% Freq, Getter and Setter
        function obj = set.Freq(obj,freq)
            if strcmpi(obj.Domain,'DISC')
                error('The FRM is on the disc, setting Freq makes no sense');
            end
            if ~FRM.checkFreq(freq)
                error('The povided frequency vector is not correct');
            end
            % make sure freq is a 1x1xF array
            freq = permute(freq(:),[3 2 1]);
            % normalise the provided frequency axis
            [~,~,obj.freq_normalised] = obj.normalisation.Forward([],[],freq);
        end
        function freq = get.Freq(obj)
            if strcmpi(obj.domain,'PLANE')
                [~,~,freq] = obj.normalisation.Backward([],[],obj.freq_normalised);
            else
                error('The FRM is on the disc, asking the frequencies doesn''t make sense')
            end
        end
        %% Theta, Getter and Setter
        function obj = set.Theta(obj,theta)
            if strcmpi(obj.Domain,'PLANE')
                error('The FRM is on the plane, setting Theta makes no sense');
            end
            if ~FRM.checkTheta(theta)
                error('The povided theta vector is not correct');
            end
            % make sure theta is a 1x1xF array
            theta = permute(theta(:),[3 2 1]);
            % set the theta axis directly
            obj.freq_normalised = theta;
        end
        function theta = get.Theta(obj)
            if strcmpi(obj.Domain,'DISC')
                theta = obj.freq_normalised;
            else
                error('The FRM is on the plane, asking the angle on the disc doesn''t make sense')
            end
        end
        %% Freq_normalised, Getter and Setter
        function obj = set.Freq_normalised(obj,freq)
            if ~obj.checkFreq(freq)
                error('the newly provided frequency axis is incorrect')
            end
            obj.freq_normalised = reshape(freq(:),[1 1 length(freq(:))]);
        end
        function res = get.Freq_normalised(obj)
            res = obj.freq_normalised;
        end
        %% Data, Getter and Setter
        function obj = set.Data(obj,data)
            if ~obj.checkData(data)
                error('provided data is not correct')
            end
            obj.data = MultiMatrix(data);
        end
        function res = get.Data(obj)
            res = obj.data;
        end
        %% Domain, Getter and Setter
        function obj = set.Domain(obj,domain)
            if ~obj.checkDomain(domain)
                error('provided domain is not correct');
            end
            switch obj.Domain
                case 'PLANE'
                    if strcmpi(domain,'DISC')
                        % perform the transformation from plane to disc
                        [obj.data,~,obj.freq_normalised] = obj.transform.Forward(obj.data,[],obj.freq_normalised);
                        % set the new domain
                        obj.domain = 'DISC';
                    end
                case 'DISC'
                    if strcmpi(domain,'PLANE')
                        % perform the transformation from disc to plane
                        [obj.data,~,obj.freq_normalised] = obj.transform.Backward(obj.data,[],obj.freq_normalised);
                        % set the new domain
                        obj.domain = 'PLANE';
                    end
                otherwise
                    error('Old domain is unknown')
            end
        end
        function res = get.Domain(obj)
            res = obj.domain;
        end
        %% NormType, Getter and Setter
        function obj = set.Normalisation(obj,normalisation)
            if ~obj.checkNormalisation(normalisation)
                error('A normalisation should implement the Normalisation class');
            end
            % denormalise the current frequency axis
            [~,~,freq] = obj.normalisation.Backward([],[],obj.freq_normalised);
            % set the new normalisation 
            obj.normalisation = normalisation;
            % set the new frequency axis
            [~,~,obj.freq_normalised] = obj.normalisation.Forward([],[],freq);
        end
        function res = get.Normalisation(obj)
            res = obj.normalisation;
        end
        %% Transform_function setter and getter
        function obj = set.Transform(obj,transform)
            if ~obj.checkTransform(transform)
                error('Provided Transform function is not correct')
            end
            obj.transform = transform;
        end
        function transform = get.Transform(obj)
            transform = obj.transform;
        end
        %%
        function obj = removeInfFrequencies(obj)
            % this function removes infinite frequencies from the FRM
            notInfFreq = ~isinf(obj.freq_normalised);
            obj.freq_normalised = obj.freq_normalised(1,1,notInfFreq);
            obj.data = obj.data(:,:,notInfFreq);
        end
        %%
        function obj = predictDC(obj)
            % calculate the predicted DC value
            tFreq = obj.Freq;
            Z_dc = padePredictDC(double(obj.data),tFreq);
            % add the DC value to the FRM
            obj.data = MultiMatrix(cat(3,Z_dc,obj.data));
            obj.Freq = cat(3,0,tFreq);
        end
        %% DISP
        function disp(obj)
            % displays the object on the command line
            [M,N,F] = size(obj);
            fprintf('%dx%dx%d FRM on the %s \n',M,N,F,obj.domain);
        end
        %% LENGTH and SIZE functions
        function res=length(obj)
            % returns the length of the FRM
            res = length(obj.data);
        end
        function varargout = size(obj,varargin)
            % returns the size of the FRM
            [varargout{1:nargout}] = size(obj.data,varargin{:});
        end
        %% OPERATORS
        function A = conj(A)
            A.data = conj(A.data);
        end
        function res = plus(A,B)
            % A + B
            res = FRM.OPERATOR(A,B,@plus);
        end
        function res = minus(A,B)
            % A - B
            res = FRM.OPERATOR(A,B,@minus);
        end
        function res = mtimes(A,B)
            % A * B
            res = FRM.OPERATOR(A,B,@mtimes);
        end
        function res = rdivide(A,B)
            % A ./ B
            res = FRM.OPERATOR(A,B,@rdivide);
        end
        function res = mldivide(A,B)
            % A \ B
            res = FRM.OPERATOR(A,B,@mldivide);
        end
        function res = mrdivide(A,B)
            % A / B
            res = FRM.OPERATOR(A,B,@mrdivide);
        end
        function res = times(A,B)
            % A .* B
            res = FRM.OPERATOR(A,B,@times);
        end
        function A = uminus(A)
            % -A
            A.data = uminus(A.data);
        end
        %% END function
        function R = end(obj,K,~)
            % used when the FRM is indexed as FRM(:,:,1:end)
            R = size(obj,K);
        end
    end
    
    methods (Static, Access = private)
        %% OPERATOR function
        % The operator function is called by TIMES PLUS, ...
        % It checks the type of the inputs and checks the frequency axes
        % before performing the provided operator
        function res = OPERATOR(A,B,opfcn)
            if isa(A,'FRM')
                if isa(B,'FRM')
                    % FRM + FRM
                    if ~FRM.compareFreqAxes(A,B)
                        error('The frequency axes of the FRMs don''t match, resample them first')
                    end
                    % sum the Data fields of both FRMs
                    A.data = opfcn(A.data,B.data);
                    res = A;
                else
                    % FRM + something else
                    % the sum function of MultiMatrix will decide whether
                    % it works or not
                    A.data = opfcn(A.data,B);
                    res = A;
                end
            else
                % obj2 is an FRM
                B.data = opfcn(A,B.data);
                res = B;
            end
        end
    end
    
    methods (Static , Access = protected)
        %% Check functions
        function tf = checkData(data)
            % validates the Data field
            tf = isnumeric(data)||isa(data,'MultiMatrix');
        end
        function tf = checkDomain(domain)
            % validates the Domain field
            tf = ismember(upper(domain),{'PLANE','DISC'});
        end
        function tf = checkNormalisation(normalisation)
            % validates the NormType field
            tf = isa(normalisation,'Normalisation');
        end
        function tf = checkDiscMax(discMax)
            % validates the DiscMax field
            tf = isscalar(discMax) && (discMax<=2*pi) && (discMax>=-2*pi);
        end
        function tf = checkFreq(freq)
            % validates the Freq field
            tf = isnumeric(freq);
        end
        function tf = checkTheta(theta)
            % validates the Theta field
            tf = isnumeric(theta) && isreal(theta) && max(abs(theta))<=2*pi;
        end
        function tf = compareFreqAxes(FRM1,FRM2)
            % compares the frequency axes of two FRMs
            if ~((length(FRM1)==1)||length(FRM2)==1)
				if strcmp(FRM1.domain,FRM2.domain)
					switch FRM1.domain
					case 'DISC'
						tf = all(abs(FRM1.Theta - FRM2.Theta)<10*eps);
					case 'PLANE'
						tf = all(abs(FRM1.Freq  - FRM2.Freq )<10*eps);
					otherwise
						error('unknown domain')
					end
				else
					error('The domain of the FRMs should be the same')
				end
            else
                tf = true;
            end
        end
        function tf = checkTransform(transform)
            % verify that func is a function handle
            tf = isa(transform,'Transform');
        end
    end
end

