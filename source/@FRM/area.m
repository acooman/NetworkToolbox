function hand=area(varargin)
% area Area plots an FRM object
% 
%      hand = area(Obj);
%      hand = area(Obj,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Obj      check: @(x) isa(x,'FRM')
%      Object that should be plotted.
% 
% Parameter/Value pairs:
% - 'Scale'     default: 'HZ'  check: @(x) ismember(upper(x),validScales)
%      Scale to use for the frequency axis. This can be either 'HZ'
%      'KHZ' 'MHZ'  or 'GHZ'.
% 
% Outputs:
% - hand      Type: handle
%      figure handle to the created plot
% 
%   C=AREA(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData')
%   creates two subplots side by side plotting the real and the imaginary 
%   parts of the data in A with a dashed red line and set the name 'myData'
%   s a legend entry.
%   
%   The following plotting modes can be used:
%     m : Modulus
%     s : Square modulus
%     a : Phase in radians
%     w : Phase wrap to [-2Pi 0]
%     d : Decibels
%     D : Phase in degrees
%     R : Real part
%     I : Imaginary part
% 



%% Parse input parameters
validScales={'HZ','KHZ','MHZ','GHZ'};
p=inputParser;
% Object that should be plotted.
p.addRequired('Obj',@(x) isa(x,'FRM'));
% Scale to use for the frequency axis. This can be either 'HZ' 'KHZ' 'MHZ'
% or 'GHZ'.
p.addParameter('Scale','HZ',@(x) ismember(upper(x),validScales));
p.KeepUnmatched = 1;
p.parse(varargin{:});
args=p.Results;

switch args.Obj.domain
    case 'PLANE'
        freq = args.Obj.Freq(:);
        % Format frequency axis
        if ismember('Scale',p.UsingDefaults)
            if args.Obj.fMax>1e9
                args.Scale='GHZ';
            elseif args.Obj.fMax>1e6
                args.Scale='MHZ';
            elseif args.Obj.fMax>1e3
                args.Scale='KHZ';
            end
        end
        switch upper(args.Scale)
            case 'GHZ'
                Xaxis=freq*1e-9;
            case 'MHZ'
                Xaxis=freq*1e-6;
            case 'KHZ'
                Xaxis=freq*1e-3;
            case 'HZ'
                Xaxis=freq;
            otherwise
                error('Unknown scale.');
        end        
        Xlabel = ['Frequency (',args.Scale,')'];
    case 'DISC'
        Xaxis = args.Obj.Theta(:);
        Xlabel = 'Angle on the disc (rad)';
    otherwise
        error('domain unknown')
end

%% PLOT
hand=area(args.Obj.Data,'Xaxis',Xaxis,'Xlabel',Xlabel,p.Unmatched);

% @generateFunctionHelp

% @tagline Area plots an FRM object

% @example C=AREA(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData')
% @example creates two subplots side by side plotting the real and the imaginary 
% @example parts of the data in A with a dashed red line and set the name 'myData'
% @example s a legend entry.
% @example
% @example The following plotting modes can be used:
% @example   m : Modulus
% @example   s : Square modulus
% @example   a : Phase in radians
% @example   w : Phase wrap to [-2Pi 0]
% @example   d : Decibels
% @example   D : Phase in degrees
% @example   R : Real part
% @example   I : Imaginary part

% @output1 figure handle to the created plot
% @outputType1 handle

