function [obj1, obj2]=intersectAxis(obj1,obj2)
% Creates an FRM with is a combination of the two frequency axes and resamples
%
% The number of frequency points in f is computed as the average between the 
% number of points in fA and the number of points in fB.
%
% 	f1=max([min(fA), min(fB)])
% 	f2=min([max(fA), max(fB)])
% 	nfp=0.5*(length(fA)+length(fB))
%   f=linspace(f1,f2,nfp)
%

fa = obj1.Freq;
fb = obj2.Freq;

f1=max([min(fa),min(fb)]);
f2=min([max(fa),max(fb)]);

% Compute the number of points for the final frequency
fa(fa<f1)=[];
fb(fb<f1)=[];
fa(fa>f2)=[];
fb(fb>f2)=[];

nfp=0.5*(length(fa)+length(fb));

if f1>f2
error('The input object are not defined within a commun frequency range');
else
f=linspace(f1,f2,nfp);
end

obj1 = resample(obj1, f);
obj2 = resample(obj2, f);
