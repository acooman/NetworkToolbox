function hand=plot(varargin)
% plot plots an FRM object
% 
%      hand = plot(Obj);
%      hand = plot(Obj,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Obj      check: @(x) isa(x,'FRM')
%      Object that should be plotted.
% 
% Parameter/Value pairs:
% - 'Scale'     default: 'HZ'  check: @(x) ismember(upper(x),validScales)
%      Scale to use for the frequency axis. This can be either 'HZ'
%      'KHZ' 'MHZ'  or 'GHZ'.
% - 'Normalised'     default: false  check: @islogical
%      When this is set to true, the normalised frequency axis is used
%      in the  plotting.
% 
% Outputs:
% - hand      Type: handle
%      figure handle to the created plot
% 
%   C=PLOT(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData')
%   creates two subplots side by side plotting the real and the imaginary 
%   parts of the data in A with a dashed red line and set the name 'myData'
%   s a legend entry.
%   
%   The following plotting modes can be used:
%     m : Modulus
%     s : Square modulus
%     a : Phase in radians
%     w : Phase wrap to [-2Pi 0]
%     d : Decibels
%     D : Phase in degrees
%     R : Real part
%     I : Imaginary part
% 


%% Parse input parameters
validScales={'HZ','KHZ','MHZ','GHZ'};
p=inputParser;
% Object that should be plotted.
p.addRequired('Obj',@(x) isa(x,'FRM'));
% Scale to use for the frequency axis. This can be either 'HZ' 'KHZ' 'MHZ'
% or 'GHZ'.
p.addParameter('Scale','HZ',@(x) ismember(upper(x),validScales));
% When this is set to true, the normalised frequency axis is used in the
% plotting.
p.addParameter('Normalised',false,@islogical);
p.KeepUnmatched = 1;
p.parse(varargin{:});
args=p.Results;

if args.Normalised
    Xaxis = args.Obj.Freq_normalised;
    Xlabel = 'Normalised frequency (a.u.)';
else
    switch args.Obj.domain
        case 'PLANE'
            freq = args.Obj.Freq(:);
            switch upper(args.Scale)
                case 'GHZ'
                    Xaxis=freq*1e-9;
                    case 'MHZ'
                    Xaxis=freq*1e-6;
                case 'KHZ'
                    Xaxis=freq*1e-3;
                case 'HZ'
                    Xaxis=freq;
                otherwise
                    error('Scale factor unknown');
            end
            Xlabel = ['Frequency (',args.Scale,')'];
        case 'DISC'
            Xaxis = args.Obj.Theta(:);
            Xlabel = 'Angle in the disc (rad)';
        otherwise
            error('domain unknown')
    end
end

%% PLOT
hand=plot(args.Obj.Data,'Xaxis',Xaxis,'Xlabel',Xlabel,p.Unmatched);

end


% @generateFunctionHelp

% @tagline plots an FRM object

% @example C=PLOT(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData')
% @example creates two subplots side by side plotting the real and the imaginary 
% @example parts of the data in A with a dashed red line and set the name 'myData'
% @example s a legend entry.
% @example
% @example The following plotting modes can be used:
% @example   m : Modulus
% @example   s : Square modulus
% @example   a : Phase in radians
% @example   w : Phase wrap to [-2Pi 0]
% @example   d : Decibels
% @example   D : Phase in degrees
% @example   R : Real part
% @example   I : Imaginary part

% @output1 figure handle to the created plot
% @outputType1 handle
