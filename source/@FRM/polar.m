function hand=polar(varargin)
% TOUCHSTONE/PLOT  Plot scattering parameters.
%
%   C=PLOT(A(i,i),Unit) plot the element (i,i) of the object A in the units 
%   specified by the argument Unit and returns a cell array with the handle 
%   to each plotted line. Unit may be one of the following strings:
%
%       m : Modulus
%       s : Square modulus
%       a : Phase in radians
%       w : Phase wrap to [-2Pi 0]
%       d : Decibels
%       D : Phase in degrees
%       R : Real part
%       I : Imaginary part
%
% 
%   C=PLOT(A,Unit) plot all vectors in A (A(1,1), A(1,2), A(2,1) ...)
%
%   C=PLOT(A,Unit,'Prop1',value,'Prop2',value) plot the data in A with the 
%   correspondent units and pass all Name-value pairs of properties to the
%   plot function. 
%
%   NOTE 1: Unit can be a 2D-char-array to obtain multiple subplots
%
%   'RI' :  Provides two plots arranged in column with real and imaginary parts
%   'dm' :  Provides two plots arranged in column with the modulus in
%           db and the modulus in linear scale.
%   ['dm';'ar']: Provides a 2x2 plots matrix with modulus in
%           db and  linear in the first row, and phase in degrees and
%           radians in the second row.
%
%   Example:
%
%   C=PLOT(A(1,1),'RI','Color','r','LineStyle','--','DisplayName','myData') 
%   creates two subplots side by side plotting the real and the imaginary 
%   parts of the data in A with a dashed red line and set the name 'myData'
%   as a legend entry.
%
%
%   NOTE 2: For convenience, it is better to specify the desired order for
%   the 'Color' property and the 'LineStyle' property in order not to
%   specify it every time. This order is defined by the ColorOrder and 
%   LineStyleOrder properties of the axes. For instance, to set the default
%   LineStyleOrder do:
%   
%   >> set(groot,'defaultAxesLineStyleOrder',{'-*',':','o'})
%
%   and to set the desired color order (already nice by default) do:
%
%   >> set(groot,'defaultColorOrder',{'r','b','y', ...})
%
%
%   See also:
%
%   Author: David Martinez <mtnez.david@gmail.com>


%% Parse input parameters
p=inputParser;
p.addRequired('Obj',@(x) isa(x,'FRM'));
p.KeepUnmatched = 1;
p.parse(varargin{:});
args=p.Results;

%% POLAR PLOT
hand=polar(args.Obj.Data,p.Unmatched);


