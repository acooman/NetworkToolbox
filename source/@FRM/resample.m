function obj=resample(obj,Xnew,varargin)
% resample resamples the FRM from one frequency axis to another
% 
%      obj = resample(Xnew);
%      obj = resample(Xnew,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Xnew      check: @(x) isnumeric(x)&&isvector(squeeze(x))
%      new frequency axis to which the FRM will be resampled
% 
% Parameter/Value pairs:
% - 'Method'     default: 'SPLINE'  check: @(x) any(strcmpi(x,{'SPLINE','PADE'}))
%      Interpolation method that will be used
% - 'Extrapolation'     default: 'INTERPOLATION'  check: @(x) any(strcmpi(x,{'INTERPOLATION','ZERO'}))
%      Extrapolation method. When this parameter is set to 'INTERPOLATION',
%      the  extrapolation is performed with the same method as the
%      interpolation.  When this is set to 'ZERO', all extrapolated
%      values are set to zero
% - 'Normalised'     default: false  check: @islogical
%      If the Normalised parameter is set to true, the new frequency
%      axis is  considered normalised, so Freq_normalised will be used
%      as a reference  X-axis. If Normalised is set to false, Freq
%      or Theta will be used
% 
% Outputs:
% - obj      Type: FRM
%      resampled FRM
% 

p = inputParser();
% new frequency axis to which the FRM will be resampled
p.addRequired('Xnew',@(x) isnumeric(x)&&isvector(squeeze(x)))
% Interpolation method that will be used
p.addParameter('Method','SPLINE',@(x) any(strcmpi(x,{'LINEAR','SPLINE','RATIONAL'})));
% Extrapolation method. When this parameter is set to 'INTERPOLATION', the
% extrapolation is performed with the same method as the interpolation.
% When this is set to 'ZERO', all extrapolated values are set to zero
p.addParameter('Extrapolation','INTERPOLATION',@(x) any(strcmpi(x,{'INTERPOLATION','ZERO'})));
% If the Normalised parameter is set to true, the new frequency axis is
% considered normalised, so Freq_normalised will be used as a reference
% X-axis. If Normalised is set to false, Freq or Theta will be used
p.addParameter('Normalised',false,@islogical);
p.parse(Xnew,varargin{:});
args = p.Results;
clear Xnew varargin

% make sure Xnew is a vector -> needed 1x1xN vector
args.Xnew=reshape(args.Xnew,1,1,[]);

% get the existing data and x-axis
Y = double(obj.Data);
if args.Normalised
    X = obj.Freq_normalised;
else
    switch obj.Domain
        case 'PLANE'
            X = obj.Freq;
        case 'DISC'
            X = obj.Theta;
        otherwise
            error('domain unknown')
    end
end

%% stuff to do with the extrapolation
switch upper(args.Extrapolation)
    case 'ZERO'
        inbandBins = (args.Xnew<=(max(X)+eps))&(args.Xnew>=(min(X)-eps));
    case 'INTERPOLATION'
        % The logical index must contain "true", not "ones" because if it
        % contains "ones" you get only the first element several times
        inbandBins = true(size(args.Xnew));
    otherwise
        error('Extrapolation method unknown')
end

%% the actual interpolation
if length(obj)==1
    % if there is only one frequency point, just repeat that point
    Ynew = repmat(Y,[1 1 length(args.Xnew)]);
else
    Ynew = zeros(size(Y,1),size(Y,2),length(args.Xnew));
    switch upper(args.Method)
        case 'LINEAR'
            M = LinearInterpolation('Domain',obj.Domain);
            M = estimate(M,X,Y);
            Ynew(:,:,inbandBins) = evaluate(M,args.Xnew(inbandBins));
        case 'SPLINE'
            Ynew(:,:,inbandBins) = spline(X,Y,args.Xnew(inbandBins));
        case 'RATIONAL'
            M = RationalInterpolation('Domain',obj.Domain);
            M = estimate(M,X,Y);
            Ynew(:,:,inbandBins) = evaluate(M,args.Xnew(inbandBins));
        otherwise
            error('Interpolation method unknown')
    end
end

% assign the obtained result to the object
obj.Data = MultiMatrix(Ynew);
% and set the new frequency axis
if args.Normalised
    obj.Freq_normalised = args.Xnew;
else
    switch obj.Domain
        case 'PLANE'
            obj.Freq = args.Xnew;
        case 'DISC'
            obj.Theta = args.Xnew;
        otherwise
            error('Unknown domain')
    end
end

% Sometimes matlab convert the data to row or column, this is a bug,
% however if it happens we put it again as 1x1xF
if isrow(obj.Data)||iscolumn(obj.Data)
    obj.Data=permute(obj.Data(:),[3 2 1]);
end

end

% @generateFunctionHelp
% @tagline resamples the FRM from one frequency axis to another
% @output1 resampled FRM
% @outputType1 FRM



