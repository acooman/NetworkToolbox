function varargout = subsref(obj,s)
%   Redefine index reference for bracket notation
%
%   DATA = OBJ(a,b,c): Pass reference to data DATA = OBJ.Data(a,b,c) and
%   to the frequency Freq = OBJ.Freq(c)
%
%   DATA = OBJ(a,b): Pass reference to data returning all elements in the
%   third dimension, DATA = OBJ.Data(a,b,:).
%
%   DATA = OBJ(a):  Return the diagonal element for all slices in the third
%   dimension, DATA = OBJ.Data(a,a,:).
%
%   In all cases the object retourned is still a FRM object.
%
%   See also:

switch s(1).type
    case '()' % when round brackets are used, an FRM is returned
        obj.Data = subsref(obj.Data,s(1));
        if length(s(1).subs)==3
            s(1).subs{1}=1;
            s(1).subs{2}=1;
            switch obj.Domain
                case 'PLANE'
                    obj.Freq = subsref(obj.Freq,s(1));
                case 'DISC'
                    obj.Theta = subsref(obj.Theta,s(1));
                otherwise
                    error('domain unknown')
            end
        end
        if length(s)>1
            obj = subsref(obj,s(2:end));
        end
        varargout{1}=obj;
    case '.'
        if any(strcmp(s(1).subs,properties(obj))) || any(strcmp(s(1).subs,methods(obj)))
            [varargout{1:nargout}] = builtin('subsref',obj,s);
        else
            error('''%s'' is not a public property or method of the ''MultiMatrix'' class.',s(1).subs);
        end
    otherwise
        [varargout{1:nargout}] = builtin('subsref',obj,s);
end
end
