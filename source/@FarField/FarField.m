classdef FarField < FRM
    %Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Dependent)
        % Theta angle
        Elev
        % Phi angle
        Azim
        % Phase shift of each port
        Phase
    end
    properties (Access=protected)
        % Theta angle
        elev
        % Phi angle
        azim
        % Phase shift of each port
        phase
    end
    methods
        %% Constructor
        function obj = FarField(Data,varargin)
            % first call the superclass constructor
            obj@FRM(Data,varargin{:});
            % check if object class and add input parser otherwise
            if isa(Data,'FarField')
                obj.theta = Data.Theta;
                obj.phi = Data.Phi;
                obj.phase = Data.Phase;
            else
                p=inputParser;
                p.KeepUnmatched = 1;
                p.StructExpand = true;
                p.PartialMatching = true;
                p.FunctionName = 'FarField';
                p.addParameter('Elev',  [],@(x) validateattributes(x,{'numeric'},{'row'}));
                p.addParameter('Azim',[],@(x) validateattributes(x,{'numeric'},{'row'}));
                p.addParameter('Phase',[],@(x) FarField.checkPhase(x,obj.Data));
                p.parse(varargin{:})
                args = p.Results;
                % Check number of outputs
                if size(obj.Data,2)~= 2*(length(args.Elev)*length(args.Azim))
                    error('Number of output ports must be twice the lenght of Theta times the lenght of Phi');
                end
                
                % set the arguments
                obj.elev  = reshape(args.Elev,[],1);
                obj.azim  = reshape(args.Azim,1,[]);
                obj.phase = reshape(args.Phase,1,[]);
            end
        end
                
        %% GET METHODS
        function prop = get.Elev(obj)
            prop = obj.elev;
        end
        function prop = get.Azim(obj)
            prop = obj.azim;
        end
        function prop = get.Phase(obj)
            prop = obj.phase;
        end
        %% SET METHODS
        function obj = set.Elev(obj,prop)
            obj.elev = prop;
        end
        function obj = set.Azim(obj,prop)
            obj.azim = prop;
        end
        function obj = set.Phase(obj,prop)
            obj.phase = prop;
        end      
        
        function [P,dP] = poynting(obj,system,dersys)
            NA = length(obj.Azim);
            NE = length(obj.Elev);
            E = obj.Data;
            S = system.Data;
            % Compute output waves
            B = exp(1i*obj.phase) * S * E;
            % Compute power
            P = abs(B).^2;
            % Sum theta and phi component
            P = (P(:,1:NA*NE,:) + P(:,NA*NE+1:end,:))./(120*pi);
            % Reshape in theta and phi
            P = reshape(P,NE,NA,[]);
            if nargout ==2
                % Repmat for each parameters
                NF = length(obj.Freq);
                NP = size(dersys,3)/NF;
                NS = size(dersys,2);
                % Multiply incident waves
                dersys = exp(1i*obj.phase) * dersys;
                % Reshape derivatives in columns (this avoid repmat)
                dersys = reshape(dersys,NS*NF,NP).';
                dersys = reshape(dersys,NP,NS,NF);
                % Compute derivatives
                dB = dersys * E;
                % Derivative of modulus square
                dP = 2 * real(conj(B) .* dB);
                % Sum theta and phi components
                dP = (dP(:,1:NA*NE,:) + dP(:,NA*NE+1:end,:))./(120*pi);
                % Reshape in theta and phi
                dP = permute(dP,[2,3,1]);
                dP = reshape(dP,NE,NA,[]);
            end
        end
        function [varargout] = eval_efficiency(obj,varargin)
            % Remove last angle (need same size as differential vector)
            NE = length(obj.Elev)-1;
            NA = length(obj.Azim)-1;
            NF = length(obj.Freq);
            % Compute poynting vector (return derivatives if necessary)
            [P{1:nargout}] = obj.poynting(varargin{:});
            % Diferential of elevation and azimut
            de = obj.Elev(2:end)-obj.Elev(1:end-1);
            da = obj.Azim(2:end)-obj.Azim(1:end-1);
            % Diferential of angle and input power
            dA = sin(obj.Elev(2:end)).*da.*de ./length(obj.phase);
            % Sum and reshape Efficiency
            varargout = cellfun(@(x) reshape(sum(reshape(x(2:end,2:end,:).*dA,NA*NE,[]),1),NF,[]),P,'UniformOutput',false);
        end
        % Evaluate function fun to obtain the component that will be added
        % to the component struct (this should happen inside interconnect)
        function varargout = efficiency(obj,x,fun,comp,netlist)
            % Obtain variable components and derivatives
            [varcomp{1:nargout}] = fun(x,obj.Freq);
            % Copy components to comp structure
            fields = fieldnames(varcomp{1});
            for ff=1:length(fields) %#ok<FORFLG>
                comp.(fields{ff}) = varcomp{1}.(fields{ff});
            end
            % Check if derivatives required
            np = size(obj.Data,1);
            if nargout>1
                [system{1:2}] = interconnect(comp,netlist,'derivatives',varcomp{2});
                system{2} = system{2}(1:end-np,np+1:end,:);
            else
                [system{1:1}] = interconnect(comp,netlist);
            end
            % Get only transmission parameters toward the antenna
            system{1} = system{1}.S(1:end-np,np+1:end,:);
            % Compute efficiency
            [varargout{1:nargout}] = obj.eval_efficiency(system{:});
            % transpose and multiply by -1 to fit fminimax format
            varargout = cellfun(@(x) x.',varargout,'UniformOutput',false);
        end
    end
    
    methods (Static)
        
        [varargout] = maxefficiency(varargin)
        
        function farfield = read(foldername,phase)
        obj = readFarField(foldername);
        % Sizes
        obj.Freq = reshape(obj.Freq,[],length(phase));
        obj.Freq = obj.Freq(:,1);
        NF = length(obj.Freq);
        NT = length(obj.Theta);
        NP = length(obj.Phi);
        NI = length(phase);
        siz = [NT,NP,NF,NI];
        % Reshape
        obj.ETHETA= permute(reshape(obj.ETHETA,siz),[4,1,2,3]);
        obj.EPHI  = permute(reshape(obj.EPHI,siz),[4,1,2,3]);
        arg.Elev  = reshape(obj.Theta,1,[])*pi/180;
        arg.Azim  = reshape(obj.Phi,1,[])*pi/180;
        arg.phase = reshape(phase,1,[]);
        arg.Freq  = obj.Freq;
        Data  = [reshape(obj.ETHETA,NI,NT*NP,NF),reshape(obj.EPHI,NI,NT*NP,NF)];
        farfield = FarField(Data,arg);
        end
    end
    
    methods (Static , Access = protected)          
        % overwrite the checkData function of MultiMatrix
        function t = checkData(data)
            t = MultiMatrix.checkData(data);
        end
        function t = checkPhase(phase,data)
            t = length(phase)==size(data,1);
        end
    end
end
