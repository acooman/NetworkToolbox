classdef MultiPort < FRM
    %  MULTIPORT:  	Subclass of FRM where the amount of inputs and outputs is
    %		equal. The S-parameters of the multiport and the reference
    %		impedance are saved in the Data field. Y and Z parameters
    %		can be calculated from these easily
    
    properties (Dependent)
        % reference impedance
        Ref
        % Z Z-parameters of the circuit
        Z
        % S S-parameters of the circuit
        S
        % Y Y-parameters of the circuit
        Y 	
    end
    properties (Access=protected)
        % reference impedance
        ref (1,1) double = 50;
    end
    methods
        function obj = MultiPort(Data,varargin)            
            % first call the superclass constructor
            obj@FRM(Data,varargin{:});
            % check if object class and add input parser otherwise
            if isa(Data,'MultiPort')
                obj.ref = Data.Ref;
            else
                p=inputParser;
                p.KeepUnmatched = 1;
                p.StructExpand = true;
                p.PartialMatching = true;
                p.FunctionName = 'MultiPort';
                p.addParameter('Zref',50 ,@obj.checkRef);
                p.addParameter('Type','S',@obj.checkType);
                p.parse(varargin{:})
                args = p.Results;
                % set the reference impedance
                obj.ref = args.Zref;
                % set the data
                obj.(args.Type) = obj.data;
            end
        end
        %% Getters and Setters for the dependent properties
        function S = get.S(obj)
            S = FRM(obj);
        end
        function obj = set.S(obj,S)
            obj.Data = S;
        end
        function obj = set.Z(obj,Z)
            % convert from Y to S and set the data in the object
            obj.data = MultiPort.convert(MultiMatrix(Z),'Z','S',obj.ref);
        end
        function Z = get.Z(obj)
            % convert from S to Y and return the matrix
            Z = MultiPort.convert(FRM(obj),'S','Z',obj.ref);
        end
        function obj = set.Y(obj,Y)
            % convert from Y to S and set the data in the object
            obj.data = MultiPort.convert(MultiMatrix(Y),'Y','S',obj.ref);
        end
        function Y = get.Y(obj)
            % convert from S to Y and return the matrix
            Y = MultiPort.convert(FRM(obj),'S','Y',obj.ref);
        end
        function obj = set.Ref(obj,Zref)
            % convert from one reference impedance to another
            obj = changeRef(obj,Zref);
        end
        function Zref = get.Ref(obj)
            Zref = obj.ref;
        end
        %% DISP function
        function disp(obj)
            % displays the MultiPort on the command line
            [M,N,F]=size(obj);
            fprintf('%dx%dx%d MultiPort on the %s\n\n',M,N,F,obj.domain);
        end
        %% Write function
        function write(obj,varargin)
            % writes the MultiPort to a touchstone file
            validTypes={'S','Y','Z','G','H','T'};
            p=inputParser;
            p.KeepUnmatched = 1;
            p.addParameter('Ref',obj.Ref,@isvector);
            p.addParameter('Type','S',@(x) ismember(upper(x),validTypes));
            p.addParameter('Name',inputname(1),@ischar);
            p.parse(varargin{:});
            args = p.Results;
            if args.Ref ~= obj.Ref
                obj = obj.changeRef(args.Ref);
            end            
            writesNp(obj.(args.Type).Data,obj.Freq,args,p.Unmatched);
        end
    end
    
    methods (Static)
        function obj = read(filename)
            % reads a touchstone file and creates a MultiPort
            param = readsNp(filename);
            if size(param.Data,1)==2
                obj = TwoPort(param.Data,param);
            else
                obj = MultiPort(param.Data,param);
            end
        end
    end
    
    methods (Static , Access = protected)  
        % make sure that the convert function is static
        res = convert(varargin);
        
        % overwrite the checkData function of MultiMatrix
        function t = checkData(data)
            % checks the Data field
            % first call the original checkData function
            if MultiMatrix.checkData(data)
                % now verify that the data is square
                t = size(data,1)==size(data,2);
            else
                t = false;
            end
        end
        function t = checkRef(ref)
            % checks the reference impedance
            t = isnumeric(ref) && isvector(ref);
        end
        function t = checkType(Type)
            % checks the Type
            t = any(strcmpi(Type,{'S','Y','Z'}));
        end
    end
end

