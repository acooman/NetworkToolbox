function [C]=chain(A,B,Np)

% MULTIPORT/CHAIN  Compute the matrix  C resulting of chaining A with B.
%
%   [C,MAP]=CHAIN(A,B) computes the matrix of the system C defined
%   as in the following structure:
%            ____________________________________________________
%           |                      System C                      |
%           |        ____________            ____________        |
%           |       |            |          |            |       |
%  Port 1___|    1__|  System A  |__2   1___|  System B  |__2    |___Port 2
%           |       |            |          |            |       |
%           |       |____________|          |____________|       |
%           |____________________________________________________|
%
%
%   C=CHAIN(A,B,n) computes scattering matrix of the system C by chaining
%   the last n port of system A with the first n ports of system B as in
%   the following schematic:
%            ____________________________________________________
%           |                      System C                      |         
%           |        ____________            ____________        |
%           |       |            |__2 - 1___|            |       |
%  Port 1___|    1__|  System A  |          |  System B  |__3    |___Port 2  
%           |       |            |__3 - 2___|            |       |
%           |       |____________|          |____________|       |
%           |____________________________________________________|
%
%   In this example, system A and B have 3 ports and n=2. The output object
%   C will have 2 ports (ports in A + ports in B - 2*n).
%
%   Chain operation: the resulting scattering matrix is computed as follows
%
%        / A(11),  0 \     / A(12)*k1,    0 \    / B(11)*A(21),    B(12) \
%   C = |             | + |                  |* |                         |
%        \ 0,   B(22)/     \ 0,    B(21)*k2 /    \ A(21),     A(22)*B(12)/
%
%   where  matrices A and B are partitioned in 4 sub-matrices such as A(22)
%   and B(11) are square of size equal to the number of ports to chain:
%           A=[A(11), A(12; A(21), A(22)];
%           B=[B(11), B(12; B(21), B(22)];
%
%   and     k1=(eye(n)-B(11)*A(22))^(-1)
%           k2=(eye(n)-A(22)*B(11))^(-1)
%
%
%
%   See also MULTIPORT/CHAIN,
%
%   Author: David Martinez <mtnez.david@gmail.com>


%% check the number of frequency points in both input matrices
if ~isequal(A.Freq,B.Freq)
	[A,B] = intersectAxis(A,B);
end

%% check reference
if ~isequal(A.Ref,B.Ref)
	B = changeRef(B,A.Zref);
end

%% Cast objects to multiport
A = MultiPort(A);
B = MultiPort(B);

%% Cascade operation
A.Data = cascade(A.Data,B.Data,Np);

%% Check number of ports
if size(A.Data,1)==2 && size(A.Data,2)==2
    C = TwoPort(A);
else
    C = MultiPort(A);
end
end

function Sc = cascade(Sa,Sb,Np)
Na=size(Sa,1);
Nb=size(Sb,1);
nfp=size(Sa,3);
%% Partition matrices and perform chaining operation
% logical index for each submatrix
a1= true(1,Na-Np);
a2=[false(1,Na-Np),true(1,Np)];
b1= true(1,Np);
b2=[false(1,Np),true(1,Nb-Np)];
% Compute matrices K1 and K2
    I = eye(Np);
    K1=I-Sb(b1,b1,:)*Sa(a2,a2,:);
    K2=I-Sa(a2,a2,:)*Sb(b1,b1,:);
% Build matrix terms
    T1=[Sa(a1,a1,:),            zeros(Na-Np,Nb-Np,nfp); ...
	zeros(Nb-Np,Na-Np,nfp), Sb(b2,b2,:)];
    T2=[Sa(a1,a2,:)/K1,       	zeros(Na-Np,Np,nfp); ...
	zeros(Nb-Np,Np,nfp),    Sb(b2,b1,:)/K2];
    T3=[Sb(b1,b1,:)*Sa(a2,a1,:),Sb(b1,b2,:); ... 
	Sa(a2,a1,:),            Sa(a2,a2,:)*Sb(b1,b2,:)];
% Chain operation
    Sc=T1+T2*T3;
end

