function obj = changeRef(obj,ref)
if ~obj.checkRef(ref)
    error('Invalid reference impedance');
end

% get the amount of ports of the circuit
P = size(obj,1);

% Change reference impedance.
r=(ref-obj.ref)/(ref+obj.ref);
obj.data = (obj.data-r*eye(P))/(eye(P)-r.*obj.data);

% Asign properties to output object
obj.ref = ref;
end
