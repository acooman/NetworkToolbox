function res = convert(obj,typeIn,typeOut,Ref)
% CONVERT changes circuit representation
%
%   res = convert(obj,typeIn,typeOut,Ref)
%
% obj     is an object that is a subclass of MultiMatrix 
% typeIn  is a string that contains the input type
% typeOut is a string that contains the output type
% Ref     is the reference impedance

N = size(obj,1);

% get the reference impedance 
Z = Ref;
Y = 1./Z;
% set the wave factor k
k = 1/(2*sqrt(real(Z)));

% build the P matrix that describes the transform
switch upper(typeIn)
    case 'Y'
        switch upper(typeOut)
            case 'Y'
                P = eye(2*N);
            case 'Z'
                P = [zeros(N) eye(N);eye(N) zeros(N)];
            case 'G'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'H'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'A'
                P = [0 0 1 0;1 0 0 0;0 0 0 1;0 -1 0 0];
            case 'B'
                P = [0 0 0 1;0 -1 0 0;0 0 1 0;1 0 0 0];
            case 'S'
                P = k*[-Z*eye(N) eye(N);Z*eye(N) eye(N)];
            case 'T'
                P = k*[0 -Z 0 1;0 Z 0 1; Z 0 1 0;-Z 0 1 0];
        end
    case 'Z'
        switch upper(typeOut)
            case 'Y'
                P = [zeros(N) eye(N);eye(N) zeros(N)];
            case 'Z'
                P = eye(2*N);
            case 'G'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'H'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'A'
                P = [1 0 0 0;0 0 1 0;0 1 0 0;0 0 0 -1];
            case 'B'
                P = [0 1 0 0;0 0 0 -1;1 0 0 0;0 0 1 0];
            case 'S'
                P = k*[eye(N) -Z*eye(N);eye(N) Z*eye(N)];
            case 'T'
                P = k*[0 1 0 -Z;0 1 0 Z;1 0 Z 0;1 0 -Z 0];
        end
    case 'G'
        switch upper(typeOut)
            case 'Y'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'Z'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'G'
                P = eye(2*N);
            case 'H'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'A'
                P = [0 0 1 0;1 0 0 0;0 1 0 0;0 0 0 -1];
            case 'B'
                P = [0 1 0 0;0 0 0 -1;0 0 1 0;1 0 0 0];
            case 'S'
                P = k*[-Z 0 1 0;0 1 0 -Z;Z 0 1 0;0 1 0 Z];
            case 'T'
                P = k*[0 1 0 -Z;0 1 0 Z;Z 0 1 0;-Z 0 1 0];
        end
    case 'H'
        switch upper(typeOut)
            case 'Y'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'Z'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'G'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'H'
                P = eye(2*N);
            case 'A'
                P = [1 0 0 0;0 0 1 0;0 0 0 1;0 -1 0 0];
            case 'B'
                P = [0 0 0 1;0 -1 0 0;1 0 0 0;0 0 1 0];
            case 'S'
                P = k*[1 0 -Z 0;0 -Z 0 1;1 0 Z 0;0 Z 0 1];
            case 'T'
                P = k*[0 -Z 0 1;0 Z 0 1;1 0 Z 0;1 0 -Z 0];
        end
    case 'A'
        switch upper(typeOut)
            case 'Y'
                P = [0 1 0 0;0 0 0 -1;1 0 0 0;0 0 1 0];
            case 'Z'
                P = [1 0 0 0;0 0 1 0;0 1 0 0;0 0 0 -1];
            case 'G'
                P = [0 1 0 0;0 0 1 0;1 0 0 0;0 0 0 -1];
            case 'H'
                P = [1 0 0 0;0 0 0 -1;0 1 0 0;0 0 1 0];
            case 'A'
                P = eye(2*N);
            case 'B'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'S'
                P = k*[1 -Z 0 0;0 0 1 Z;1 Z 0 0;0 0 1 -Z];
            case 'T'
                P = k*[0 0 1 Z;0 0 1 -Z;1 Z 0 0;1 -Z 0 0];
        end
    case 'B'
        switch upper(typeOut)
            case 'Y'
                P = [0 0 0 1;0 -1 0 0;0 0 1 0;1 0 0 0];
            case 'Z'
                P = [0 0 1 0;1 0 0 0;0 0 0 1;0 -1 0 0];
            case 'G'
                P = [0 0 0 1;1 0 0 0;0 0 1 0;0 -1 0 0];
            case 'H'
                P = [0 0 1 0;0 -1 0 0;0 0 0 1;1 0 0 0];
            case 'A'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'B'
                P = eye(2*N);
            case 'S'
                P = k*[0 0 1 -Z;1 Z 0 0;0 0 1 Z;1 -Z 0 0];
            case 'T'
                P = k*[1 Z 0 0;1 -Z 0 0;0 0 1 Z;0 0 1 -Z];
        end
    case 'S'
        switch upper(typeOut)
            case 'Y'
                P = 1/2/k*[-Y*eye(N) Y*eye(N);eye(N) eye(N)];
            case 'Z'
                P = 1/2/k*[eye(N) eye(N);-Y*eye(N) Y*eye(N)];
            case 'G'
                P = 1/2/k*[-Y 0 Y 0;0 1 0 1;1 0 1 0;0 -Y 0 Y];
            case 'H'
                P = 1/2/k*[1 0 1 0;0 -Y 0 Y;-Y 0 Y 0;0 1 0 1];
            case 'A'
                P = 1/2/k*[1 0 1 0;-Y 0 Y 0;0 1 0 1;0 Y 0 -Y];
            case 'B'
                P = 1/2/k*[0 1 0 1;0 Y 0 -Y;1 0 1 0;-Y 0 Y 0];
            case 'S'
                P = eye(2*N);
            case 'T'
                P = [0 1 0 0;0 0 0 1;0 0 1 0;1 0 0 0];
        end
    case 'T'
        switch upper(typeOut)
            case 'Y'
                P = 1/2/k*[0 0 Y -Y;-Y Y 0 0;0 0 1 1;1 1 0 0];
            case 'Z'
                P = 1/2/k*[0 0 1 1;1 1 0 0;0 0 Y -Y;-Y Y 0 0];
            case 'G'
                P = 1/2/k*[0 0 Y -Y;1 1 0 0;0 0 1 1;-Y Y 0 0];
            case 'H'
                P = 1/2/k*[0 0 1 1;-Y Y 0 0;0 0 Y -Y;1 1 0 0];
            case 'A'
                P = 1/2/k*[0 0 1 1;0 0 Y -Y;1 1 0 0;Y -Y 0 0];
            case 'B'
                P = 1/2/k*[1 1 0 0;Y -Y 0 0;0 0 1 1;0 0 Y -Y];
            case 'S'
                P = [0 0 0 1;1 0 0 0;0 0 1 0;0 1 0 0];
            case 'T'
                P = eye(2*N);
        end
end

% cut the P matrix into parts
c1 = [true(1,N), false(1,N)];
c2 = ~c1;
% Cut the matrix
P11 = P(c1,c1);
P12 = P(c1,c2);
P21 = P(c2,c1);
P22 = P(c2,c2);

% and perform the conversion
res = ( P11*obj + P12 )/( P21*obj + P22 );



end