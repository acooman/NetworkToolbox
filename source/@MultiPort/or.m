function C=or(A,B)
    C=parallel(A,B);
end

function [A]=parallel(A,B)

% MULTIPORT/PARALLEL Compute the matrix  C resulting of parallel A with B.
%
%   C=PARALLEL(A,B) parallel connection of objects A and B as follows:
%            ______________________________________
%           |               System C               |         
%           |             ____________             |
%           |            |            |            | 
%           |  Port 1 ___|  System A  |___ Port 2  | 
%           |        |   |            |   |        |
%           |        |   |____________|   |        |
%  Port 1___|________|                    |________|___Port 2 
%           |        |    ____________    |        |
%           |        |   |            |   |        |
%           |        |___|  System B  |___|        | 
%           |  Port 1    |            |    Port 2  |
%           |            |____________|            |
%           |                                      |
%           |______________________________________|
%
%
%
%
%
%   See also MULTIPORT/CHAIN, MULTIPORT/SERIES.
%
%   Author: David Martinez <mtnez.david@gmail.com>



%% check the number of frequency points in both input matrices
if ~isequal(A.Freq,B.Freq)
	[A,B] = intersectAxis(A,B);
end

%% compute both objects with the same reference
if ~isequal(A.Zref,B.Zref)
	B = changeRef(B,A.Zref);
end

%% Compute output matrix
A.Y=A.Y + B.Y


end

