function [A]=series(A,B)

% MULTIPORT/SERIES  Compute the matrix  C resulting of chaining A with B.
%
%   C=SERIES(A,B) series connection of objects A and B as follows:
%            ____________________________
%           |          System C          |         
%           |        ____________        |
%        ___|_______|            |_______|___ 
%           |       |  System A  |       | 
%           |      _|            |_      |
%           |     | |____________| |     |
%  Port 1   |     |                |     |   Port 2 
%           |     |  ____________  |     |
%           |     |_|            |_|     |
%           |       |  System B  |       | 
%        ___|_______|            |_______|___
%           |       |____________|       |
%           |                            |
%           |____________________________|
%
%
%
%
%   See also MULTIPORT/CHAIN, MULTIPORT/PARALLEL.
%
%   Author: David Martinez <mtnez.david@gmail.com>

%% check the number of frequency points in both input matrices
if ~isequal(A.Freq,B.Freq)
	[A,B] = intersectAxis(A,B);
end

%% compute both objects with the same reference
if ~isequal(A.Zref,B.Zref)
	B = changeRef(B,A.Zref);
end

%% Compute output matrix
A.Z=A.Z + B.Z



end
