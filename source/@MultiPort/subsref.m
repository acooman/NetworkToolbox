function varargout = subsref(obj,s)
%   Redefine index reference for bracket notation
%
%   DATA = OBJ(a,b,c): Pass reference to data DATA = OBJ.Data(a,b,c) and
%   to the frequency Freq = OBJ.Freq(c)
%
%   DATA = OBJ(a,b): Pass reference to data returning all elements in the
%   third dimension, DATA = OBJ.Data(a,b,:).
%
%   DATA = OBJ(a):  Return the diagonal element for all slices in the third
%   dimension, DATA = OBJ.Data(a,a,:).
%
%   In Number of ports equals 2 object retourned is a TwoPort object,
%   otherwise a MultiPort is retourned.
%
%   See also:

switch s(1).type
    case '()'
        if length(s(1).subs(1))~=1
            error('Some funny error.');
        end
        if isa(obj,'TwoPort')
            obj = MultiPort(obj);
        end
        varargout{1} = subsref@FRM(obj,s);
        if isa(varargout{1},'MultiPort')
            % if the obtained result is a twoport, cast it to a twoport
            if size(varargout{1}.Data,1)==2
                varargout{1} = TwoPort(varargout{1});
            elseif isa(obj,'TwoPort')
                % I dont see whats the point in this line ???
                varargout{1} = MultiPort(varargout{1});
            end
        end
    otherwise
        [varargout{1:nargout}] = builtin('subsref',obj,s);
end


end

