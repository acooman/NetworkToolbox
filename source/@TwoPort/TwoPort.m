classdef TwoPort < MultiPort
    %  TWOPORT:  	Subclass of MultiPort where the amount of ports is equal to two
    properties (Dependent)
        % H H-parameters of the circuit
        H
        % G G-parameters of the circuit
        G
        % A A-parameters of the circuit
        A
        % B B-parameters of the circuit
        B
        % T T-parameters of the circuit
        T
    end
    
    methods
        %% CONSTRUCTOR
        function obj = TwoPort(varargin)
            % Constructor
            % Just call the superclass constructor
            % the check functions are overwritten,
            % so it will do what we want, :) NICE
            obj@MultiPort(varargin{:});
        end
        function disp(obj)
            % displays the object on the command line
            [M,N,F]=size(obj);
            fprintf('%dx%dx%d TwoPort on the %s\n\n',M,N,F,obj.domain);
        end
        
        %% G parameters
        function res = get.G(obj)
            res = MultiPort.convert(FRM(obj),'S','G',obj.ref);
        end
        function obj = set.G(obj,NewData)
            % call the convert function on the new data and store it in the
            obj.data = MultiPort.convert(MultiMatrix(NewData),'G','S',obj.ref);
        end
        %% H parameters
        function res = get.H(obj)
            res = MultiPort.convert(FRM(obj),'S','H',obj.ref);
        end
        function obj = set.H(obj,NewData)
            % call the convert function on the new data and store it in the
            obj.data = MultiPort.convert(MultiMatrix(NewData),'H','S',obj.ref);
        end
        %% A parameters
        function res = get.A(obj)
            res = MultiPort.convert(FRM(obj),'S','H',obj.ref);
        end
        function obj = set.A(obj,NewData)
            % call the convert function on the new data and store it in the
            obj.data = MultiPort.convert(MultiMatrix(NewData),'A','S',obj.ref);
        end
        %% B parameters
        function res = get.B(obj)
            res = MultiPort.convert(FRM(obj),'S','B',obj.ref);
        end
        function obj = set.B(obj,NewData)
            % call the convert function on the new data and store it in the
            obj.data = MultiPort.convert(MultiMatrix(NewData),'B','S',obj.ref);
        end
        %% T parameters
        function res = get.T(obj)
            res = MultiPort.convert(FRM(obj),'S','T',obj.ref);
        end
        function obj = set.T(obj,NewData)
            % call the convert function on the new data and store it in the
            obj.data = MultiPort.convert(MultiMatrix(NewData),'T','S',obj.ref);
        end
        
    end
    
    methods (Static , Access = protected)
        % overwrite the checkType function
        function t = checkType(Type)
            % checks the possible Types
            t = ismember(upper(Type),{'S','T','Y','Z','G','H','A','B'});
        end
        % overwrite the checkData function
        function t = checkData(Data)
            % check the Data field
            % first call the superclass checkData function
            % then verify that it is a twoport
            t = MultiPort.checkData(Data) && size(Data,1)==2;
        end
    end
end

