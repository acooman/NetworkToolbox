classdef BandpassNormalisation < Normalisation

    properties
        Fcenter (1,1) double {mustBeReal}
        Fbandwidth (1,1) double {mustBeReal}
    end
    
    methods
        function obj = BandpassNormalisation(varargin)
            % call the normalisation constructor
            obj = obj@Normalisation(varargin{:});
            % compute Fcenter and Fbandwidth
            obj.Fcenter    = (obj.Fmin + obj.Fmax)/2;
            obj.Fbandwidth = (obj.Fmax - obj.Fmin)/2;
            % avoid Fbandwidth being zero
            if obj.Fbandwidth==0
                obj.Fbandwidth = 1;
            end
        end
    end
    
%% implement the Transform functions
	methods (Access = protected)
        % functions to transform the parameter values
        function freq_normalised=F_par(obj,freq)
			freq_normalised = (freq-obj.Fcenter)./obj.Fbandwidth;
		end
        function freq=B_par(obj,freq_normalised)
			freq = freq_normalised.*obj.Fbandwidth+obj.Fcenter;
        end
        % functions to transform the complex values
        function [s_normalised]=F_var(obj,s)
			s_normalised = (s-1i*2*pi*obj.Fcenter)./obj.Fbandwidth;
		end
        function [s]=B_var(obj,s_normalised)
			s = s_normalised.*obj.Fbandwidth+1i*2*pi*obj.Fcenter;
        end
        % functions to transform a realisation
        function [An,Bn,Cn,Dn]=F_abcd(obj,A,B,C,D)
            c = 1i*2*pi*obj.Fcenter;
            b = obj.Fbandwidth;
            An = A./b-c./b*eye(size(A));
            Bn = B./sqrt(b);
            Cn = C./sqrt(b);
            Dn = D;
        end
        function [A,B,C,D]=B_abcd(obj,An,Bn,Cn,Dn)
            c = 1i*2*pi*obj.Fcenter;
            b = obj.Fbandwidth;
            A = b.*An+c.*eye(size(An));
            B = sqrt(b).*Bn;
            C = sqrt(b).*Cn;
            D = Dn;
        end
    end

end