classdef LowpassNormalisation < Normalisation
    methods
        function obj = LowpassNormalisation(varargin)
            % call the normalisation constructor
            obj = obj@Normalisation(varargin{:});
            % set fMin to zero and fMax
            obj.Fmax = max([-obj.Fmin obj.Fmax]);
            obj.Fmin = -obj.Fmax;
        end
    end
%% implement the Transform functions
	methods (Access = protected)
        % functions to transform the parameter values
        function freq_normalised=F_par(obj,freq)
			freq_normalised = (freq)./obj.Fmax;
		end
        function freq=B_par(obj,freq_normalised)
			freq = freq_normalised.*obj.Fmax;
        end
        % functions to transform the complex values
        function [s_normalised]=F_var(obj,s)
			s_normalised = (s)./obj.Fmax;
		end
        function [s]=B_var(obj,s_normalised)
			s = s_normalised.*obj.Fmax;
        end
        % functions to transform a realisation
        function [An,Bn,Cn,Dn]=F_abcd(obj,A,B,C,D)
            a = obj.Fmax;
            An = A./a;
            Bn = B./sqrt(a);
            Cn = C./sqrt(a);
            Dn = D;
        end
        function [A,B,C,D]=B_abcd(obj,An,Bn,Cn,Dn)
            a = obj.Fmax;
            A = An.*a;
            B = Bn.*sqrt(a);
            C = Cn.*sqrt(a);
            D = Dn;
        end
    end
end