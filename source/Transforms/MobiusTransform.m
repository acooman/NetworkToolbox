classdef MobiusTransform < Transform
    %MOBIUSTRANSFORM2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        alpha (1,1) double {mustBeReal}
        theta_0 (1,1) double {mustBeReal}
        isometry (1,1) logical
    end
    
    methods
        function obj = MobiusTransform(varargin)
            p = inputParser;
            % This sets the angle on the unit disc to which the origin of the complex
            % plane is mapped.
            p.addParameter('DCMappedTo' ,pi  ,@(x)isnumeric(x)&&isscalar(x))
            % this indicates the circle segment on the unit circle to which the
            % interval [0,1] or [0,j] is mapped.
            p.addParameter('ZeroOneInterval',3*pi/4,@(x)isnumeric(x)&&isscalar(x))
            % This controls which half-plane is mapped to the unit disc. When
            % AxisMappedToCircle is 'IMAG', you can choose between the Left Half-Plane
            % (LHP) or the right half-plane (RHP)
            % when AxisMappedToCircle is 'REAL', you can choose between the upper
            % half-plane (UHP) or down half-plane (DHP)
            p.addParameter('InsideHalfPlane','RHP',@(x) ismember(upper(x),{'LHP','RHP'}));
            % set this to true when you want the transform to maintain the L2 norm.
            p.addParameter('isometry',false,@islogical);
            % p.addParameter('alpha',2*pi*1/cot(pi/4/2),@isscalar)
            p.parse(varargin{:})
            args = p.Results;
            % get theta_a and theta_b for the transform
            obj.theta_0 = args.DCMappedTo;
            switch upper(args.InsideHalfPlane)
                case 'LHP'
                    n=1;
                case 'RHP'
                    n=-1;
                otherwise
                    error('When the Real axis mapped to the disc, you can choose "UHP" or "DHP"')
            end
            % calculate the alpha coefficient
            obj.alpha = 2*pi*cot((n*args.ZeroOneInterval)/2);
        end
    end
    %% Implement the abstract methods from the Transform class
    methods (Access=protected)
        % functions to transform the parameter values
        function theta=F_par(obj,freq)
            theta = 2*atan(2*pi*freq./obj.alpha)+obj.theta_0;
        end
        function freq=B_par(obj,theta)
            freq = obj.alpha/2/pi*tan((theta-obj.theta_0)/2);
            % make sure that, when theta-theta_a = pi that the result is inf.
            freq(abs(abs(theta-obj.theta_0)-pi)<3*eps)=inf;
        end
        % functions to go from parameter to complex variable
        function s = par2var_o(obj,freq)
            s = 1i*2*pi*freq;
        end
        function z = par2var_d(obj,theta)
            z = exp(1i*theta);
        end
        % functions to transform the complex values
        function z=F_var(obj,s)
            z=exp(1i*obj.theta_0)*(obj.alpha+s)./(obj.alpha-s);
        end
        function s=B_var(obj,z)
            s=obj.alpha*(z-exp(1i*obj.theta_0))./(z+exp(1i*obj.theta_0));
        end
        % functions to transform the function values
        function Zdisc=F_fun(obj,s,Zplane)
            if obj.isometry
                error('I haven''t implemented the isometry yet')
            else
                Zdisc = Zplane;
            end
        end
        function Zplane=B_fun(obj,z,Zdisc)
            if obj.isometry
                error('I haven''t implemented the isometry yet')
            else
                Zplane = Zdisc;
            end
        end
        % function to transform the A,B,C,D matrices
        function [Ad,Bd,Cd,Dd] = F_abcd(obj,Ap,Bp,Cp,Dp)
            I = eye(size(Ap));
            a = obj.alpha;
            E = exp(1i*obj.theta_0);
            P = (I-Ap./a);
            Ad = E*(P\(I+Ap./a));
            fac = sqrt(2./a*E);
            Bd = fac*(P\Bp);
            Cd = fac*(Cp/P);
            Dd = Dp + Cp*(P\Bp)./a;
        end
        function [Ap,Bp,Cp,Dp] = B_abcd(obj,Ad,Bd,Cd,Dd)
            I = eye(size(Ad));
            E = exp(-1i*obj.theta_0);
            a = obj.alpha;
            P = (E*Ad+I);
            Ap = a*(P\(E*Ad-I));
            fac = sqrt(2*a*E);
            Bp = fac*(P\Bd);
            Cp = fac*(Cd/P);
            Dp = Dd - E*Cd*(P\Bd);
        end
    end
end

