classdef Normalisation < Transform
    
    properties
        % Minumum frequency of the unnormalised interval. During the normalisation, this frequency is mapped to -1
        Fmin (1,1) double {mustBeReal}
        % Maximum frequency of the unnormalised interval. During the normalisation, this frequency is mapped to +1
        Fmax (1,1) double {mustBeReal}
    end
    
    methods
        function obj = Normalisation(varargin)
            switch nargin
                case 1
                    % a frequency vector is provided by the user, find its minimum and maximum
                    Fmin = min(varargin{1});
                    Fmax = max(varargin{1});
                case 2
                    % fmin and fmax are provided directly by the user
                    Normalisation.checkFmin(varargin{1});
                    Normalisation.checkFmax(varargin{2});
                    Fmin = varargin{1};
                    Fmax = varargin{2};
            end
            % call the superclass constructor
            obj = obj@Transform();
            % set the data
            obj.Fmin = Fmin;
            obj.Fmax = Fmax;
        end
        
        function test(obj)
            % run the transform tests
            test@Transform(obj);
            % check whether Fmin is mapped to -1
            [~,~,t] = obj.Forward([],[],obj.Fmin);
            if abs(t-(-1))>1e-6
                error('Fmin is not mapped to -1');
            end
            % check whether Fmax is mapped to +1
            [~,~,t] = obj.Forward([],[],obj.Fmax);
            if abs(t-(+1))>1e-6
                error('Fmax is not mapped to +1');
            end
            disp('Normalisation tests passed.')
        end
    end
    
    methods (Access = protected)
        % functions to go from parameter to complex variable
        function [s] = par2var_o(obj,freq)
            s = 1i*2*pi*freq;
        end
        function [s_normalised] = par2var_d(obj,freq_normalised)
            s_normalised = 1i*2*pi*freq_normalised;
        end
        % functions to transform the function values
        function [g]=F_fun(obj,v,f)
            g=f;
        end
        function [f]=B_fun(obj,w,g)
            f=g;
        end
    end
    
    methods (Static , Access = protected)
        % validates the FMin field
        function checkFmin(Fmin)
            validateattributes(Fmin,{'numeric'},{'scalar','real','finite','nonnan'});
        end
        % validates the FMax field
        function checkFmax(Fmax)
            validateattributes(Fmax,{'numeric'},{'scalar','real','finite','nonnan'});
        end
    end
end