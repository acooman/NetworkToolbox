# Transform abstraction

The `Transform` class is an abstraction of the general transforms used on FRMs to go from the complex plane to the unit disc. Examples include the Bilinear transform and the more general Mobius transform.

We start with the FRM $`f\left(v\right)\in\mathbb{C}^{N\times M}`$
defined on a trace in the complex plane $`v\left(p\right)\in\mathbb{C}`$
parametrised by $`p\in\mathbb{R}`$.

We transform this to a new FRM $`g\left(w\right)\in\mathbb{C}^{N\times M}`$
defined on an trace $`w\left(p\right)\in\mathbb{C}`$ parametrised by
$`q\in\mathbb{R}`$.

The abstract class `Transform` needs the implementation of the following functions:

|            Original | Forward                             | Backward                            | Destination         |
|--------------------:|:-----------------------------------:|:-----------------------------------:|:--------------------|
|               $`p`$ | `q=F_par(p)`                        | `p=B_par(q)`                        | $`q`$               |
|    `w=par2var_o(p)` |                                     |                                     | `v=par2var_d(q)`    |
|               $`w`$ | `v=F_var(w)`                        | `w=B_var(v)`                        | $`v`$               |
|               $`f`$ | `g=F_fun(f,w)`                      | `f=B_fun(g,v)`                      | $`g`$               |
| $`A_o,B_o,C_o,D_o`$ | `[Ad,Bd,Cd,Dd]=F_abcd(Ao,Bo,Co,Do)` | `[Ao,Bo,Co,Do]=B_abcd(Ad,Bd,Cd,Dd)` | $`A_d,B_d,C_d,D_d`$ |

It then combines these functions into a `Forward` and `Backward` function which allows to transform between the two domains:

```matlab
[g,w,q] = Transform.Forward(f,v,p)
[f,v,p] = Transform.Backward(g,w,q)
```

State-space realisations are transformed using the following formulas

```matlab
[Ad,Bd,Cd,Dd] = Transform.Forward_ABCD(Ao,Bo,Co,Do)
[Ao,Bo,Co,Do] = Transform.Backward_ABCD(Ad,Bd,Cd,Dd)
```

