classdef Transform
    %TRANSFORM defines any transform between two versions of the complex plane
    %
    % In the original domain, we have 
    %       a function f 
    %       evaluated on a line in the complex plane v
    %       which is parametrised by a real variable p
    %       a realisation Ao,Bo,Co,Do such that f = Co*(v*I-Ao)^-1*Bo+Do
    %
    % This is transformed to
    %       a function g
    %       evaluated on a line in the complex plane w
    %       which is parametrised by a real variable p
    %       a realisation Ad,Bd,Cd,Dd such that g = Cd*(w*I-Ad)^-1*Bd+Dd
    %
    % The transform class then gives two functions to go from one domain to another
    %
    %   [g,w,q] = Transform.Forward(f,v,p)
    %   [f,v,p] = Transform.Backward(q,w,q)
    %
    % And two functions to transform the realisations
    %
    %   [Ad,Bd,Cd,Dd] = Transform.Forward_ABCD(Ao,Bo,Co,Do)
    %   [Ao,Bo,Co,Do] = Transform.Backward_ABCD(Ad,Bd,Cd,Dd)
    %
    % These functions use a collection of abstract methods that should be
    % implemented by a specific transform:
    %
    % functions to transform the parameter values
    %   q = F_par(obj,p);
    %   p = B_par(obj,q);
    % functions to go from parameter to complex variable
    %   v = par2var_o(obj,p);
    %   w = par2var_d(obj,q);
    % functions to transform the complex values
    %   w = F_var(obj,v);
    %   v = B_var(obj,w);
    % functions to transform the function values
    %   g = F_fun(obj,v,f);
    %   f = B_fun(obj,w,g);
    % functions to transform state-space realisations
    %   [Ad,Bd,Cd,Dd]=F_abcd(obj,Ao,Bo,Co,Do);
    %   [Ao,Bo,Co,Do]=B_abcd(obj,Ad,Bd,Cd,Dd);
    %
    
    methods
        function [g,w,q] = Forward(obj,f,v,p)
            % Forward transform, from the target domain back to the original one
            %
            %   [f,v,p] = Transform.Forward(g,w ,q);
            %   [f,v,p] = Transform.Forward(g,[],q);
            %   [f,v] = Transform.Forward(g,w);
            % 
            % where 
            %   g   is the function in the transformed domain
            %   w   are the complex numbers in the target domain on which g is evaluated
            %   q   are real numbers which parametrise the w-curve in the complex plane
            %
            %   f   is thf function in the original domain
            %   v   are the complex numbers in the original domain on which f is evaluated
            %   p   are real numbers which parametrise the v-curve in the complex plane
            switch nargin
                case {1,2}
                    error('not enough inputs')
                case 3
                    % check the inputs
                    validateattributes(v,{'numeric'},{'3d','size',[1,1,NaN]});
                    if ~isempty(f)
                        F = size(v,3);
                        validateattributes(f,{'numeric'},{'3d','size',[NaN,NaN,F]});
                    end
                    % compute the transform
                    switch nargout
                        case 1
                            if ~isempty(f)
                                g = F_fun(obj,v,f);
                            else
                                g=[];
                            end
                        case 2
                            if ~isempty(f)
                                g = F_fun(obj,v,f);
                            else
                                g=[];
                            end
                            w = F_var(obj,v);
                        case 3
                            error('cant compute parameter')
                        otherwise     
                            error('too many outputs')
                    end
                case 4
                    % check the inputs
                    validateattributes(p,{'numeric'},{'real','3d','size',[1,1,NaN]});
                    F = size(p,3);
                    if ~isempty(v)
                        validateattributes(v,{'numeric'},{'3d','size',[1,1,F]});
                    end
                    if ~isempty(f)
                        validateattributes(f,{'numeric'},{'3d','size',[NaN,NaN,F]});
                    end
                    % compute the transform
                    switch nargout
                        case 1
                            if ~isempty(f)
                                g = F_fun(obj,v,f);
                            else
                                g=[];
                            end
                        case 2
                            if ~isempty(f)
                                g = F_fun(obj,v,f);
                            else
                                g=[];
                            end
                            w = F_var(obj,v);
                        case 3
                            q = F_par(obj,p);
                            if isempty(v)
                                v = par2var_d(obj,p);
                            end
                            w = F_var(obj,v);
                            if ~isempty(f)
                                g = F_fun(obj,v,f);
                            else
                                g=[];
                            end
                        otherwise
                            error('too many outputs')
                    end
                otherwise
                    error('too many inputs')
            end
        end        
        
        function [f,v,p] = Backward(obj,g,w,q)
            % Backward transform, from the target domain back to the original one
            %
            %   [f,v,p] = Transform.Backward(g,w ,q);
            %   [f,v,p] = Transform.Backward(g,[],q);
            %   [f,v] = Transform.Backward(g,w);
            % 
            % where 
            %   g   is the function in the transformed domain
            %   w   are the complex numbers in the target domain on which g is evaluated
            %   q   are real numbers which parametrise the w-curve in the complex plane
            %
            %   f   is thf function in the original domain
            %   v   are the complex numbers in the original domain on which f is evaluated
            %   p   are real numbers which parametrise the v-curve in the complex plane
            switch nargin
                case {1,2}
                    error('not enough inputs')
                case 3
                    % check the inputs
                    validateattributes(w,{'numeric'},{'3d','size',[1,1,NaN]});
                    if ~isempty(g)
                        F = size(w,3);
                        validateattributes(g,{'numeric'},{'3d','size',[NaN,NaN,F]});
                    end
                    % compute the transform
                    switch nargout
                        case 1
                            if ~isempty(g)
                                f = B_fun(obj,w,g);
                            else
                                f=[];
                            end
                        case 2
                            if ~isempty(g)
                                f = B_fun(obj,w,g);
                            else
                                f=[];
                            end
                            v = B_var(obj,w);
                        case 3
                            error('cant compute parameter')
                        otherwise     
                            error('too many outputs')
                    end
                case 4
                    % check the inputs
                    validateattributes(q,{'numeric'},{'real','3d','size',[1,1,NaN]});
                    F = size(q,3);
                    if ~isempty(w)
                        validateattributes(w,{'numeric'},{'3d','size',[1,1,F]});
                    end
                    if ~isempty(g)
                        validateattributes(g,{'numeric'},{'3d','size',[NaN,NaN,F]});
                    end
                    % compute the transform
                    switch nargout
                        case 1
                            if ~isempty(g)
                                f = B_fun(obj,w,g);
                            else
                                f=[];
                            end
                        case 2
                            if ~isempty(g)
                                f = B_fun(obj,w,g);
                            else
                                f=[];
                            end
                            v = B_var(obj,w);
                        case 3
                            p = B_par(obj,q);
                            if isempty(w)
                                w = par2var_d(obj,q);
                            end
                            v = B_var(obj,w);
                            if ~isempty(g)
                                f = B_fun(obj,w,g);
                            else
                                f=[];
                            end
                        otherwise
                            error('too many outputs')
                    end
                otherwise
                    error('too many inputs')
            end
        end
        %% Methods to transform a state-space realisation
        function [A,B,C,D] = Forward_ABCD(obj,A,B,C,D)
            % check the inputs
            
            % call the abstract function to compute the result
            [A,B,C,D] = F_abcd(obj,A,B,C,D);
        end
        function [A,B,C,D] = Backward_ABCD(obj,A,B,C,D)
            % check the inputs
            
            % call the abstract function to compute the result
            [A,B,C,D] = B_abcd(obj,A,B,C,D);
        end
        %% Method to test the implementation of this class
        function test(obj)
            % this function tests the implementation of the implemented functions
            F = 500;
            tol = 1e-12;
            w = sort(rand(1,1,F),3);
            % check whether B_par is the inverse of F_par
            t = obj.F_par(w);
            if max(abs( w - obj.B_par(t) ))>tol
                error('there seems to be an error in the B_par or F_par implementation')
            end
            s = obj.par2var_o(w);
            % check whether B_var is the inverse of F_var
            z = obj.F_var(s);
            if max(abs( s - obj.B_var( z ) ))>tol
                error('there seems to be an error in the B_var, F_var or par2var_o implementation')
            end
            % check whether par2var_d(q) makes sense
            if max(abs( s - obj.B_var(obj.par2var_d(t))))>tol
                error('there seems to be an error in the par2var_d implementation')
            end
            % check whether the data is properly transformed
            Zp = rand(2,3,F)+1i*rand(2,3,F);
            Zd = obj.F_fun(s,Zp);
            ftest = obj.B_fun(z,Zd);
            if max(abs(Zp(:)-ftest(:)))>tol
                error('there is something wrong with B_fun or F_fun')
            end
            % check the transformation of a state-space model
            O = 5;
            Ap = rand(O)+1i*rand(O);
            Bp = rand(O,2)+1i*rand(O,2);
            Cp = rand(3,O)+1i*rand(3,O);
            Dp = rand(3,2)+1i*rand(3,2);
            % apply the state-space transform and its inverse
            [Ad,Bd,Cd,Dd]=F_abcd(obj,Ap,Bp,Cp,Dp);            
            [Aptest,Bptest,Cptest,Dptest]=B_abcd(obj,Ad,Bd,Cd,Dd);
            % check whether the transform and its inverse match
            if max(abs(Aptest(:)-Ap(:)))>tol
                error(['The A-matrix is not maintained while transforming an back: max(abs(A-Atest))=' num2str(max(abs(Aptest(:)-Ap(:))))]);
            end
            if max(abs(Bptest(:)-Bp(:)))>tol
                error(['The B-matrix is not maintained while transforming an back: max(abs(B-Btest))=' num2str(max(abs(Bptest(:)-Bp(:))))]);
            end
            if max(abs(Cptest(:)-Cp(:)))>tol
                error(['The C-matrix is not maintained while transforming an back: max(abs(C-Ctest))=' num2str(max(abs(Cptest(:)-Cp(:))))]);
            end
            if max(abs(Dptest(:)-Dp(:)))>tol
                error(['The D-matrix is not maintained while transforming an back: max(abs(D-Dtest))=' num2str(max(abs(Dptest(:)-Dp(:))))]);
            end
            % evaluate the models in a bunch of data points
            Zp = double(Cp*((MultiMatrix(s).*eye(O)-Ap)\Bp) + Dp);
            Zd = double(Cd*((MultiMatrix(z).*eye(O)-Ad)\Bd) + Dd);
            % check whether the response of the transformed system matches the transformed response
            Zdtest = F_fun(obj,s,Zp);
            if max(abs( Zd(:) - Zdtest(:) ))>tol
                figure(654321)
                clf
                hold on
                set(gca,'ColorOrderIndex',1);
                for uu=1:size(Dp,2)
                    plot(squeeze(t),db(squeeze(Zdtest(:,uu,:))));
                end
                set(gca,'ColorOrderIndex',1);
                for uu=1:size(Dp,2)
                    plot(squeeze(t),db(squeeze(Zd    (:,uu,:))),'.');
                end
                error('The response of the transformed state-space realisation does not match the transform of the response of the state-space representation')
            end
            disp('Transform tests passed.')
        end
    end
    %% Abstract methods that should be implemented by the transform
    methods (Abstract,Access=protected)
        % functions to transform the parameter values
        [q]=F_par(obj,p);
        [p]=B_par(obj,q);
        % functions to go from parameter to complex variable
        [v] = par2var_o(obj,p);
        [w] = par2var_d(obj,q);
        % functions to transform the complex values
        [w]=F_var(obj,v);
        [v]=B_var(obj,w);
        % functions to transform the function values
        [g]=F_fun(obj,v,f);
        [f]=B_fun(obj,w,g);
        % functions to transform a state space transformation
        [a,b,c,d]=F_abcd(obj,a,b,c,d);
        [a,b,c,d]=B_abcd(obj,a,b,c,d);
    end
end

