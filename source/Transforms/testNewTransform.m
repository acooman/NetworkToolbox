clear variables
close all
clc

% get the function handle to the transform function
% t = MobiusTransform('Inside','RHP');
t = MobiusTransform();

% run the transform self-test
t.test

% F is the amount of frequencies we take into consideration
F = 101;

% generate the frequency axis
omega = reshape(linspace(-1,2,F),1,1,[]);
s = 1i*omega;
% the data is just a bunch of zeros for the moment
data_plane = rand(2,2,F)+1i*rand(2,2,F);

% go from the plane to the disc in some different ways
[data_disc,z] = t.Forward(data_plane,s);
[~,z,theta] = t.Forward([],[],omega);
[data_disc,z,theta] = t.Forward(data_plane,[],omega);

[data_plane_test,s_test] = t.Backward(data_disc,z);
[~,s_test,omega_test] = t.Backward([],[],theta);
[data_plane_test,s_test,omega_test] = t.Backward(data_disc,[],theta);

% check that the inverse of the transform results in the same thing
if max(abs(omega-omega_test))>1e-12
    error('difference in parameter too big')
end
if max(abs(s-s_test))>1e-12
    error('difference in variable too big')
end
if max(abs(data_plane-data_plane_test))>1e-12
    error('difference in function too big')
end

%% test the implemented Normalisations
t = LowpassNormalisation(omega);
t.test();
t = BandpassNormalisation(omega);
t.test();
disp('test passed!')