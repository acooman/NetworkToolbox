# Interconnect

Function that allows to interconnect several MultiPorts to determine the resulting MultiPort.

## Usage

The interconnect function needs two inputs:

```matlab
result = interconnect( componentStruct , netlist );
```

The `netlist` is a cell array of strings that describe how the different MultiPorts are interconnected.
Each line of the `netlist` has the following syntax: 

```
NameX node1 .. nodeN | param1=value1 .. paramM=valueM
```

This line describes how MultiPort NameX is connected to node1 through nodeN. Additional parameters can be passed to the MultiPort by specifying the param,value pairs.

The `componentStruct` is a struct that contains the different MultiPorts. The name of the field in the struct should match with NameX used in the netlist.
Make sure that all MultiPorts in the `componentStruct` have the same frequency axis.

Alternatively, when the MultiPort is not present in the `componentStruct`, Interconnect will try to call the NameX function. 
When this function returns a MultiPort, this result will be used in the algorithm.

## Examples

### Simple cascade of three MultiPorts

Assume we have three TwoPorts MatchIN, MatchOUT and  AMP that describe the input/out matching networks and an amplifier respectively. We wish to compute the cascade of these three components.
First, we place the MultiPorts in the `componentStruct`:

```matlab
componentStruct.IN  = MatchIN;
componentStruct.OUT = MatchOUT;
componentStruct.AMP = AMP;
```

Now, we generate the `netlist` that describes their interconnection:

```matlab
netlist = { ...
	'IN  a b' ...
	'AMP b c' ...
	'OUT c d' ...
}
```

Finally, we call the interconnect function:

```matlab
result = interconnect( componentStruct , netlist );
```

Because the nodes 'a' and 'd' appear only once in the `netlist`, they are assumed to be outputs of the circuit. The result is therefore a TwoPort.
The order of the ports in the TwoPort is determined by the order with which the output ports appear in the netlist. 
Port 1 of the resulting TwoPort will correspond to node 'a' of the total circuit and port 2 of the result corresponds to node 'd'.

### Using the component functions

Let's connect two resistors in series. In this case, we have the following netlist:

```
R a b | R=5
R b c | R=10
```

Because R is a built-in component, the R-function will be called with the specified parameters to generate the MultiPorts. We can pass an empty struct as the `componentStruct`.
When passing an empty `componentStruct`, a frequency vector has to be provided to the interconnect function as is shown below:

```matlab
result = interconnect( struct() , netlist , 'freq',0:10 );
```

### Passing parameters as doubles

In the previous example, we passed the resistance values '5' and '10' as a string. This is easy when the numbers are integers and fixed, but sometimes we want to pass the circuit variables without turning them into strings.
We can pass variables as doubles by constructing the `netlist` in the following way:

```matlab
netlist = {...
{'R a b |' 'R' 5 }...
{'R b c |' 'R' pi}...
};
```

### Using terminations

## Algorithm

The algorithm we use is based on the 1974 paper by Vito Monaco:

> Monaco, V and Tiberio, P "Computer-Aided Analysis of Microwave Circuits" *IEEE Tran. on microwave theory and techniques*, vol. 22, no. 3, March 1974.

The following algorithm is performed for each of the frequencies on which the resulting S-matrix is to be known.

We assume that each of the components in the netlist has an S-matrix indicated by $`S_n`$. We gather these different $`S_n`$ in a large block-diagonal matrix

```math
S_\mathrm{huge} = \left[\begin{array}{cccc}
S_{1} & 0 & \cdots & 0\\
0 & S_{2} & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \cdots & S_{N}
\end{array}\right]
```

with this large S-matrix, we have the first set of equations that describe the relation between the $`a`$ and $`b`$ waves in the circuit

```math
b = S_\mathrm{huge} a
```

When two ports of different blocks are interconnected, the $`b`$ waves of one port are equal to the $`a`$ waves of the other port and vice versa. 
This equality is expressed with a $`\Gamma`$-matrix where $`\Gamma_{ij}=1`$ when ports $`i`$ and $`j`$ are connected. With the $`\Gamma`$ matrix, we have the second expression that describes the relationship bewteen the waves in the circuit:

```math
b = \Gamma a
```

We now re-order the ports of the $`S_\mathrm{huge}`$ and $`\Gamma`$ matrix such that the ports which are connected to the terminations are placed first, then come the internal ports of the circuit which are connected to each other.

The resulting $`S_\mathrm{huge}`$ can be split into 4 parts:

```math
\left[\begin{array}{c}
b_{p}\\
b_{c}
\end{array}\right]=\left[\begin{array}{cc}
S_{pp} & S_{pc}\\
S_{cp} & S_{cc}
\end{array}\right]\left[\begin{array}{c}
a_{p}\\
a_{c}
\end{array}\right]
```

where $`b_p`$, $`a_p`$ are the waves at the output ports and $`b_c`$ and $`a_c`$ are the waves at the interconnected ports.
When we apply the same transformation to the $`\Gamma`$-matrix, we obtain

```math
\left[\begin{array}{c}
b_{p}\\
b_{c}
\end{array}\right]=\left[\begin{array}{cc}
0 & 0\\
0 & \Gamma_{cc}
\end{array}\right]\left[\begin{array}{c}
a_{p}\\
a_{c}
\end{array}\right]
```

The S-matrix of the interconnected circuit is the S-matrix which links $`b_p`$ to $`a_p`$, so solving our equations for $`b_p`$ yields the result:

```math
S_\mathrm{res} = S_{pp} + S_{pc} \left( \Gamma_{cc} - S_{cc} \right)^{-1} S_{cp}
```



## Computing the derivatives

It is also possible to compute the derivative of the resulting Multiport with respect to the deviations in the components.
in that case, the interconnect function is called with two outputs and with an additional input struct that contains the derivatives:

```matlab
[res,dres]=interconnect( componentStruct , netlist , 'derivatives',dcomponentStruct )
```

**TODO:** David should write about this

## Implemented Component functions

Currently, we have implemented the following component functions:

* R (with parameter R)
* SeriesR (with parameter R)
* ShuntR (with parameter R)
* SeriesL (with parameter L) 
* ShuntL (with parameter L)
* SeriesC (with parameter C)
* ShuntC (with parameter C)
* PhaseShift (with parameter Theta)
* TL (with parameters PH, ref and fc)

When you want to add your own component function, make sure that it returns a TwoPort or a MultiPort and that it can be called in the following way:

```matlab
	res = MyFunc('freq',frequency,'paramName',paramValue,...);
```

**TODO:** David added something that makes the component return a MultiMatrix, with an extra parameter, explain how this works.

As an example, take the implementation of the SeriesR component:
```matlab
function PS = SeriesR(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'R';
p.KeepUnmatched = true;
p.addParameter('R',0,@isscalar);
p.parse(varargin{:});
args = p.Results;
% Compute S parameters
data = (1/args.R).*[1,-1;-1,1];
% Call twoport constructor
PS = TwoPort(data,'Type','Y',p.Unmatched);
end
```


## Requirements

Matlab 2016b or higher. Most of the code has been tested on Matlab 2017a
