# Interconnection of  Z and Y parameters

Suppose we have some blocks described by Z and or Y parameters.

```math
\left( \begin{array}{c}  V_{11} \\ V_{12} \end{array} \right) = \mathbf{Z}_1 \left( \begin{array}{c} I_{11}\\I_{12} \end{array} \right)
```

and

```math
\left( \begin{array}{c} V_{21}\\V_{22} \end{array} \right) = \mathbf{Z}_2 \left( \begin{array}{c} I_{21}\\I_{22} \end{array} \right)
```

and we wish to interconnect port 2 of block one with port 1 of block two, this adds two extra equations

```math
V_{12} = V_{21}\qquad  \mathrm{and} \qquad I_{12} = -I_{21}
```

we can express these four equations in a large matrix

```math
\left(  \begin{array}{c}
1 & 0 & 0 & 0 & -Z^1_{11} & -Z^1_{12}  & 0 & 0 \\
0 & 1 & 0 & 0 & -Z^1_{21} & -Z^1_{22}  & 0 & 0 \\
0 & 0 & 1 & 0 & 0 & 0  & -Z^2_{11} & -Z^2_{12} \\
0 & 0 & 0 & 1 & 0 & 0  & -Z^2_{21} & -Z^2_{22} \\
0 & 1 & -1 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 1 & 1 & 0 \\
\end{array}
\right) \left( \begin{array}{c}  V_{11} \\ V_{12} \\ V_{21} \\ V_{22}  \\ I_{11} \\ I_{12} \\ I_{21} \\ I_{22}  \end{array} \right) = 0
```

There are two external ports in this circuit. We re-order the equations in this matrix such that the voltages and currents of these external ports are placed first, then the voltages of the interconnected ports and then the currents of the interconnected ports. We obtain the following:

```math
\left(  \begin{array}{c}
1 & 0 & -Z^1_{11} & 0               & 0 & 0  & -Z^1_{12} & 0 \\ 
0 & 0 & -Z^1_{21} & 0               & 1 & 0  & -Z^1_{22} & 0 \\ 
0 & 0 & 0               & -Z^2_{12} & 0 & 1  & 0               & -Z^2_{11} \\ 
0 & 1 & 0               & -Z^2_{22} & 0 & 0  & 0               & -Z^2_{21} \\ 
0 & 0 & 0               & 0               & 1 & -1 & 0               & 0 \\ 
0 & 0 & 0               & 0               & 0 & 0  & 1                & 1 \\
\end{array}
\right) \left( \begin{array}{c}  V_{11} \\ V_{22} \\ I_{11} \\ I_{22}  \\ V_{12} \\ V_{21} \\ I_{12} \\ I_{21}  \end{array} \right) = 0
```

I think this set of equations will always be of the following form

```math
\left(  \begin{array}{c} \mathbf{A} & \mathbf{B} \\ \mathbf{0} & \mathbf{C} \end{array}\right)  \left(  \begin{array}{c} \mathbf{X} \\ \mathbf{Y} \end{array} \right) = 0
```

where

```math
\mathbf{X} = \left( \begin{array}{c} V_{11} \\ V_{22}  \\ I_{11} \\ I_{22} \end{array} \right)
```

and we need to re-write this set of equations in the following form to obtain the Z matrix of the interconnected circuit

```math
\left( \begin{array}{c} \mathbf{I}_2 &  \mathbf{Z}^t \end{array} \right) \mathbf{X}=0
```

so we need to eliminate $`Y`$ from the set. 