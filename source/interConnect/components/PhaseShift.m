function PS = PhaseShift(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'PhaseShift';
p.KeepUnmatched = true;
p.addParameter('Theta',0,@isscalar);
p.parse(varargin{:});
args = p.Results;

% Compute S parameters
data = [0 exp(1i*args.Theta);exp(1i*args.Theta), 0];

% Call twoport constructor
PS = TwoPort(data,p.Unmatched);
end