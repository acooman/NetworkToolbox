function [PS,der] = SeriesC(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'SeriesC';
p.KeepUnmatched = true;
p.addParameter('C',0,@isscalar);
p.addParameter('ref',50,@isscalar);
p.addParameter('freq',0,@isnumeric);
p.addParameter('normalised',false,@islogical);
p.addParameter('MultiMatrix',false,@islogical);

p.parse(varargin{:});
args = p.Results;

% Normalise components to avoid numeric errors
% if ~args.normalised
%     FMax = max(abs(args.freq));
% else
    FMax =1;
% end
C = args.C*FMax;
F = 2*pi*reshape(args.freq/FMax,1,1,[]);

% Polynomials
P = -1i/(2*args.ref);
R = [C, 0];
Q = [C, P];

q = polyval(Q,F);
S11 = P./q;
S21 = polyval(R,F)./q;

PS = MultiMatrix([S11, S21; S21,S11]);

if nargout==2
    der = ([0,1;1,0] - PS).*F.*FMax./q;
end

% Cast result
if ~args.MultiMatrix
    PS = TwoPort(PS);
end
end
