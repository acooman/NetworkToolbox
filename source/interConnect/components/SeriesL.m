function [PS,der] = SeriesL(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'SeriesL';
p.KeepUnmatched = true;
p.addParameter('L',0,@isscalar);
p.addParameter('ref',50,@isscalar);
p.addParameter('freq',0,@isnumeric);
p.addParameter('normalised',false,@islogical)
p.addParameter('MultiMatrix',false,@islogical);

p.parse(varargin{:});
args = p.Results;

% Normalise components to avoid numeric errors
% if ~args.normalised
%     FMax = max(abs(args.freq));
% else
    FMax =1;
% end
L = args.L*FMax;
F = 2*pi*reshape(args.freq/FMax,1,1,[]);

% Polynomials
P = [L 0];
R = -1i*2*args.ref;
Q = [L, R];

q = polyval(Q,F);
S11 = polyval(P,F)./q;
S21 = R./q;

PS = MultiMatrix([S11, S21; S21,S11]);

if nargout==2
    der = (eye(2) - PS).*F.*FMax./q; 
end

% Cast result
if ~args.MultiMatrix
    PS = TwoPort(PS);
end
end
