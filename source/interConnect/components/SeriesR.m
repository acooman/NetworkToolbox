function PS = SeriesR(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'R';
p.KeepUnmatched = true;
p.addParameter('R',0,@isscalar);
p.parse(varargin{:});
args = p.Results;

% Compute S parameters
data = (1/args.R).*[1,-1;-1,1];

% Call twoport constructor
PS = TwoPort(data,'Type','Y',p.Unmatched);
end
