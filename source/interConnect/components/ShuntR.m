function PS = ShuntR(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'R';
p.KeepUnmatched = true;
p.addParameter('R',0,@isscalar);
p.parse(varargin{:});
args = p.Results;

% Compute S parameters
data = (args.R).*ones(2);

% Call twoport constructor
PS = TwoPort(data,'Type','Z',p.Unmatched);
end
