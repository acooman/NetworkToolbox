function [PS,der] = TL(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'TransmissionLine';
p.KeepUnmatched = true;
p.addParameter('PH',0,@isscalar);
p.addParameter('ref',50,@isscalar);
p.addParameter('fc',[],@isnumeric);
p.addParameter('freq',0,@isnumeric);
p.addParameter('MultiMatrix',false,@islogical);

p.parse(varargin{:});
args = p.Results;

if isempty(args.fc)
    args.fc = (max(freq)+min(freq))/2;
end
args.freq = reshape(args.freq,1,1,[]);
S11 = zeros(1,1,length(args.freq));
S21 = exp(-1i*args.PH*args.freq./args.fc);

PS = MultiMatrix([S11, S21; S21,S11]);

if nargout==2
    der = PS ;
    dPH = -1i*args.freq./args.fc;
    der(1,2,:) = der(1,2,:).*dPH;
    der(2,1,:) = der(2,1,:).*dPH;
end

% Cast result
if ~args.MultiMatrix
    PS = TwoPort(PS,'Freq',args.freq);
end
end
