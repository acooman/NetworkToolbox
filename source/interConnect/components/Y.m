function PS = Y(varargin)
% Input Parser
p = inputParser;
p.StructExpand = true;
p.FunctionName = 'Y';
p.KeepUnmatched = true;
p.addParameter('Y',0,@isscalar);
p.parse(varargin{:});
args = p.Results;

% Compute S parameters
data = args.Y*ones(2,2);

% Call twoport constructor
PS = TwoPort(data,'Type','Z',p.Unmatched);
end