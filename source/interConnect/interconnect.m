function [RES,DER] = interconnect(componentStruct,netlist,varargin)
% INTERCONNECT connects several MultiPorts together according to a provided netlist
%
%    [result,derivative] =  INTERCONNECT(componentStruct,netlist)
%    [result,derivative] =  INTERCONNECT(componentStruct,netlist,'ParamName',ParamValue)
%
% 
%
% Required Inputs:
%   componentStruct  Default:  CheckFunction: @isstruct
%     structure which contains MultiPort objects that are used in the netlist
%   netlist  Default:  CheckFunction: @iscell
%     cell array of strings that contains the way how the multiports are
%     interconnected.
%     The ports of the result are indicated using 'Term' statements in the netlist
%     The first Term statement in the netlist corresponds to the first port of
%     the result. The second term statement gives the second port etc etc.
%     The names of the nodes in the netlist can be numbers or strings.
% Parameter-Value pairs:
%   useTerminations  Default: true CheckFunction: @islogical
%     when this parameter is set to false, the function does not require Term
%     statements to be used. Instead, every unconnected port in the netlist
%     will be a port in the result. The order of the ports in the result is
%     given by the order in which the ports appear in the original netlist.
%   performChecks  Default: true CheckFunction: @islogical
%     when this parameter is set to false, the function does not perform checks
%     on the input data to save time
%   useSparse  Default: false CheckFunction: @islogical
%     when this is set to true, the matrix inverse will be calculated using
%     sparse matrix techniques
%   evalComponents  Default: true CheckFunction: @islogical
%     when evalComponents is true, the functions related to the specified
%     component are being called.
%   frequency  Default: [] CheckFunction: @(x) isnumeric(x)&&isvector(x)
%     frequency axis which is used when there are no components in the
%     componentstruct
%   layout  Default: false CheckFunction: @islogical
%     set layout to true to show a nice graph
%   derivatives  Default: [] CheckFunction: @isstruct
%     derivatives of the component to compute jacobian. The jacobian will be
%     retourned in the second output argument
% 
% Outputs: 
%   result Type: MultiPort or TwoPort
%     MultiPort which contains the description of the total circuit.
%   derivative Type: MultiMatrix
%     MultiMatrix which contains the derivative of the obtained S-parameters to the elements in the circuit
%
% cascade of two filters
% componentStruct =
% Filter1: [2x2x100 TwoPort]
% Filter2: [2x2x100 TwoPort]
% netlist =
% {
% 'Term a';
% 'Filter1 a b';
% 'Filter2 b c';
% 'Term c';
% }
% res = interconnect(componentStruct,netlist)     
% 


p = inputParser();
p.KeepUnmatched     = true;
p.FunctionName      = 'InterConnect';
p.StructExpand      = true;
% structure which contains MultiPort objects that are used in the netlist
p.addRequired('componentStruct',@isstruct);
% cell array of strings that contains the way how the multiports are
% interconnected.
% The ports of the result are indicated using 'Term' statements in the netlist
% The first Term statement in the netlist corresponds to the first port of
% the result. The second term statement gives the second port etc etc.
% The names of the nodes in the netlist can be numbers or strings.
p.addRequired('netlist',@iscell);
% when this parameter is set to false, the function does not require Term
% statements to be used. Instead, every unconnected port in the netlist
% will be a port in the result. The order of the ports in the result is
% given by the order in which the ports appear in the original netlist.
p.addParameter('useTerminations',true,@islogical);
% when this parameter is set to false, the function does not perform checks
% on the input data to save time
p.addParameter('performChecks',true,@islogical);
% when this is set to true, the matrix inverse will be calculated using
% sparse matrix techniques
p.addParameter('useSparse',false,@islogical);
% when evalComponents is true, the functions related to the specified
% component are being called.
p.addParameter('evalComponents',true,@islogical);
% frequency axis which is used when there are no components in the
% componentstruct
p.addParameter('frequency',[],@(x) isnumeric(x)&&isvector(x));
% set layout to true to show a nice graph
p.addParameter('layout',false,@islogical)
% derivatives of the component to compute jacobian. The jacobian will be
% retourned in the second output argument
p.addParameter('derivatives',[],@isstruct)
% parse inputs
p.parse(componentStruct,netlist,varargin{:});
args = p.Results;
unmatched = p.Unmatched;
% we will work with the original variables in the function, so remove the
% netlist and componentStruct fields from args to avoid confusion
args = rmfield(args,{'netlist','componentStruct'});

%% Split the netlist at the spaces

netlist = netlist(:);
netlist_temp=cell(size(netlist));
for nn=1:length(netlist)
    if ischar(netlist{nn})
        % if this line in the netlist is a string, split it at the spaces or the equal signs
        netlist_temp{nn} = regexp(netlist{nn},'\s+|(\s*\=\s*)','split');
    elseif iscell(netlist{nn})
        % if the line in the netlist is a cell array, split all the strings
        % it contains at the spaces or the equal signs leave the other data untouched
        if ~isvector(netlist{nn})
            error('each cell array in the netlist should be a vector')
        end
        for mm=1:length(netlist{nn})
            if ischar(netlist{nn}{mm})
                % if it's a string, split it
                netlist_temp{nn}= [netlist_temp{nn} regexp(netlist{nn}{mm},'\s+|(\s*\=\s*)','split')];
            else
                % if it's something else, leave it alone
                netlist_temp{nn}= [netlist_temp{nn} netlist{nn}{mm}];
            end
        end
    else
        error('the netlist should only contain strings or cell arrays')
    end
end
% overwrite the old netlist by the new one
netlist = netlist_temp;

%% Evaluate the component functions

% get the frequency axis out of one of the components in the struct
t = fieldnames(componentStruct);
if ~isempty(t)
    args.frequency = componentStruct.(t{1}).Freq;
end

% When a certain model is not present in the componentstruct, try
% evaluating the function with that name and add it to the componentstruct
for nn=1:length(netlist)
    if ~isfield(componentStruct,netlist{nn}{1})
        if ~strcmpi(netlist{nn}{1},'Term')
            % find the spot in the list where the | sign is placed.
            % the parameters are all placed after that sign
            paramsplit = find(cellfun(@(x) all(x=='|'),netlist{nn}),1);
            % run the component function
            [res{1:nargout}] = feval(netlist{nn}{1},'freq',args.frequency,'MultiMatrix',true,netlist{nn}{paramsplit+1:end});
            % remove the parameters from the netlist
            netlist{nn}(paramsplit:end)=[];
            % check the returned result. It should be a multiport
            if ~isa(res{1},'MultiMatrix')
                error('The component function did not return a Multiport')
            end
            % The returned multiport should have the right amount of nodes
            if size(res{1},1)~=(length(netlist{nn})-1)
                error('The component returned the incorrect amount of ports')
            end
            % assign the result to a unique field of the component structure
            componentStruct.(['c' num2str(nn)]) = res{1};
            if nargout ==2
                args.derivatives.(['c' num2str(nn)]) = res{2};
            end
            % replace the name of the component by the name of the field in the struct
            netlist{nn}{1} = ['c' num2str(nn)];
        end
    end
end

%% now that the components have been evaluated, just do the normal interconnection
% get the names of all the elements
componentNames = cellfun(@(x) x{1},netlist,'UniformOutput',false);
termLines = strcmpi(componentNames,'Term');

%% Do some more checks on the componentStruct
% if args.performChecks
    % TODO: Make sure that all the MultiPorts have the same frequency axis
    % TODO: the function should allow a combination of MultiPorts and
    %       RationalMatrices. The rationalmatrices should be evaluated on the
    %       frequency grid of the multiports and then used as MultiPorts
    % UPDATE: This is now very simple. If F is a rationalFilter,
    %         then F.getResponse(Freq,'Hz') return a twoport.
    
    % check that everything in the componentStruct is a MultiPort
%     if ~all(structfun(@(x) isa(x,'MultiMatrix'),componentStruct))
%         error('The componentStruct should only contain MultiPorts');
%     end
    % check that all components specified in the netlist are in the componentStruct
    if ~all(isfield(componentStruct,componentNames(~termLines)))
        missingComponents = ~isfield(componentStruct,componentNames);
        error('The following component(s) are missing in the components structure:\n   %s',strjoin(componentNames(missingComponents),'\n   '));
    end
    % check that all elements in the netlist have the right amount of
    % ports, including the terminations (they should have only one port)
    % Get components size
    Nports = structfun(@(x) size(x,1),componentStruct,'UniformOutput',false);
    % Number of ports of terminations
    termNames = unique(componentNames(termLines));
    % Add ports of terminations to the sizes structure
    for ii = 1:length(termNames)
        Nports.(termNames{ii})=1;
    end
    % Verify number of ports
    checkports = cellfun(@(x) (length(x)-1) ~= Nports.(x{1}),netlist);
    if any(checkports)
        format = repmat('%s \n ',1,sum(checkports));
        str = cellfun(@(x) strjoin(x),netlist{checkports},'UniformOutput',false);
        error(['The number of nodes in the following statements \n\n',format,' \n does not match the amount of ports in the component structure'],str{:});
    end
% end





%% All of this need to be done before removing terminations from the list
% get a list of all the node statements
nodeList = cellfun(@(x) x(2:end),netlist,'uniformOutput',false);
nodeList = [nodeList{:}];

%% draw the graph
if args.layout
    uNodeList = unique(nodeList);
    uNodeListNum = cellfun(@hash,uNodeList);
    G = zeros(length(uNodeList));
    for cc=1:length(netlist)
        for p=2:length(netlist{cc})
            ind1 = hash(netlist{cc}{p})==uNodeListNum;
            for q=p+1:length(netlist{cc})
                ind2 = hash(netlist{cc}{q})==uNodeListNum;
                G(ind1,ind2)=1;
            end
        end
    end
    G = G+G.';
    g = graph(G,uNodeList);
    o = plot(g,'layout','layered','Direction','right');
end


%% Add junction if necessary
% Find number of ocurrences of each node
nodeListNum = cellfun(@hash,nodeList);
occur = sum(triu(nodeListNum == nodeListNum.'),1);
% if any node appears more than 2 times add junction

for i=find(occur>2)
    % find occurences
    p = find(nodeListNum == nodeListNum(i));
    % replace first 2 matches (this should be unique)
    nodeList{p(1)}= [nodeList{p(1)},'1'];
    nodeList{p(2)}= [nodeList{p(2)},'2'];
    nodeListNum(p(1)) = hash(nodeList{p(1)});
    nodeListNum(p(2)) = hash(nodeList{p(2)});
    % Add new nodes for the junction
    nodeList = [nodeList,nodeList{p(1)},nodeList{p(2)},nodeList{p(3)}];
    nodeListNum = [nodeListNum,nodeListNum(p(1)),nodeListNum(p(2)),nodeListNum(p(3))];
    % Add junction to component struct
    componentStruct.(['junction',num2str(i)]) = MultiMatrix([-1 2 2;2 -1 2;2 2 -1]/3);
    % Add number of ports of the junction
    Nports.(['junction',num2str(i)]) = occur(i);
    % Add junction name
    componentNames{end+1} = ['junction',num2str(i)];
    % SI LE DERIVE EST CALCULE, AJOUTE ZEROS
    % (No derivative with junctions for the moment)
end



% remove the terminations from the list (from nodeList as well)
if args.useTerminations
    % Recompute termLines (taking junctions into account)
    termLines = strcmpi(componentNames,'Term');
    % index in the nodelist of each component
    ind = cellfun(@(x) Nports.(x),componentNames);
    ind = cumsum([1;ind(1:end-1)]);
    % Get termNodes
    termNodes = nodeList(ind(termLines));
    termNodesNum = nodeListNum(ind(termLines));
    % Remove terminations from the componentNames
    nodeList(ind(termLines)) = []; 
    nodeListNum(ind(termLines)) = [];
    componentNames = componentNames(~termLines);
end



%% Construct the interconnection matrix GAMMA
if args.performChecks
    if ~all(any(strjoin(nodeList,'')==char(48:(75+47)).',1))
        error('one of the characters used in the nodes is outside of the supported character range');
    end
end

% the gamma matrix is now the equal of this vector with its transpose
GAMMA = sparse(nodeListNum == nodeListNum.');
% the diagonal elements should not be included in the interconnection
GAMMA = GAMMA - speye(length(nodeList));
% check for cases

if any(sum(GAMMA,1)>1)
    error('The following nodes have too many connections:\n   %s',strjoin(unique(nodeList(sum(GAMMA)>1)),'\n   '));
end


%% Gather all the MultiMatrices in a large block-diagonal MultiMatrices
% get the MultiMatrix with the S-parameters out of each of the components
% componentStructData = structfun(@(x) x.Data,componentStruct,'uniformOutput',false);
% componentStructData = componentStruct;
% convert the structure into a cell array of MultiMatrices
% components = struct2cell(componentStructData);
% the corresponding name
fields = fieldnames(componentStruct);
for ii=1:length(fields)
   if ~isa(componentStruct.(fields{ii}),'MultiMatrix')
      componentStructData.(fields{ii}) = componentStruct.(fields{ii}).Data;
   else
      componentStructData.(fields{ii}) = componentStruct.(fields{ii});
   end
end
components = struct2cell(componentStructData);

%% Find component location
componentNamesNum = cellfun(@hash,componentNames);
fieldsNum = cellfun(@hash,fields);
c = findLocations(componentNamesNum,fieldsNum);

components = components(c);
% MEME CHOSE
% gather all the MultiMatrices in a large block-diagonal matrix
Shuge = MultiMatrix.blkdiag(components{:});
% MEME CHOSE AVEC LES DERIV��ES
% Q is the total amount of ports in the circuit
Q = size(Shuge,1);

if nargout==2
    % Field names of the derivative structure
    fieldsderivatives = fieldnames(args.derivatives);
    % Number of frequencies
    NF = size(Shuge,3);
    % Number of parameters in each object
    NP = structfun(@(x) size(x,3)/NF,args.derivatives);
    % Number of parameters times number of frequencies
    nfp = sum(NP)*NF;
    if length(fieldsderivatives)>1
        % Add zeros for the derivatives of other components
        ind = cumsum([0;NP(1:end-1)]);
        for ii=1:length(fieldsderivatives)
            deri = args.derivatives.(fieldsderivatives{ii});
            args.derivatives.(fieldsderivatives{ii}) = zeros(size(deri,1),size(deri,2),nfp);
            args.derivatives.(fieldsderivatives{ii})(:,:,ind(ii)*NF+1:(ind(ii)+NP(ii))*NF) = deri;
        end
    end
    % Fields missing in the derivatives structure
    missing = find(~ismember(fields,fieldsderivatives));
    % Add zeros for any component not present in derivatives structure
    if ~isempty(missing)
        for ii=reshape(missing,1,[])
            [sm,sn,~]= size(componentStructData.(fields{ii}));
            args.derivatives.(fields{ii}) = zeros(sm,sn,nfp);
        end
    end
    % Add missing field names
    fieldsderivatives = [fieldsderivatives;fields(missing)];
    % Find locations
    fieldsderivativesNum = cellfun(@hash,fieldsderivatives);
    c = findLocations(componentNamesNum,fieldsderivativesNum);
    % convert the structure into a cell array of MultiMatrices
    args.derivatives = struct2cell(args.derivatives);
    % sort elements in the derivatives arrray
    args.derivatives = args.derivatives(c);
    DerShuge = MultiMatrix.blkdiag(args.derivatives{:});
end



%% Get the information about the Terminations and about the amount of ports in the final result
if args.useTerminations
%     termNodesNum = cellfun(@hash,termNodes);
%     nodeListNum = cellfun(@hash,nodeList);
    [c,occur] = findLocations(termNodesNum,nodeListNum);    
    % Check number of connections
    noconnections = occur==0;
    multipleconnection = occur>1;
    if any(noconnections)
        str = strjoin(nodeList{noconnections});
        error('The following terminations: " %s " are not connected to any node',str);
    end
    if any(multipleconnection)
        str = strjoin(nodeList{multipleconnection});
        error('The following terminations: " %s " connected to multiple nodes',str);
    end  

    % the solo nodes are the nodes that don't appear in a connection
    soloNodes = nodeList(~any(GAMMA,1));
    % hash the soloNodes into a unique numeric value
    soloNodesNum = cellfun(@hash,soloNodes);
    % cross-check this list by the termination node list
    cross = soloNodesNum == termNodesNum.';
    if ~all(any(cross))
        warning('The following node(s) are not connected to anything:\n   %s',strjoin(soloNodes(~any(cross)),'\n   '));
    end
else
    % when no terminations are used the solonodes are the ones that will appear in the result
    c = find(~any(GAMMA,1));
    % P is the amount of ports in the result
end
%% Number of ports in the result
    P = length(c);
%% Now finally calculate the result

% figure(32154654)
% clf
% subplot(121)
% spy(GAMMA,'r');
% hold on
% spy(squeeze(double(Shuge(:,:,1))));

% reshape the matrices such that the ports with the terminations connected to them come first
T = 1:Q;
reshuffle = [c, T(all(c.'~=T,1))];
Shuge = Shuge(reshuffle,reshuffle,:);
GAMMA = GAMMA(reshuffle,reshuffle);
% MEME CHOSE AVEC LE DERIVE

if nargout==2
    DerShuge = DerShuge(reshuffle,reshuffle,:);
end

% subplot(122)
% spy(GAMMA,'r');
% hold on
% spy(squeeze(double(Shuge(:,:,1))));

% cut the matrix into pieces
ind = [true(1,P) false(1,Q-P)];
Spp = Shuge( ind, ind,:);
Spc = Shuge( ind,~ind,:);
Scp = Shuge(~ind, ind,:);
Scc = Shuge(~ind,~ind,:);
Gcc = GAMMA(~ind,~ind);
% MEME CHOSE AVEC LE DERIVE
if nargout==2
    dSpp = DerShuge( ind, ind,:);
    dSpc = DerShuge( ind,~ind,:);
    dScp = DerShuge(~ind, ind,:);
    dScc = DerShuge(~ind,~ind,:);
end
% calculate the connection scattering matrix
if nargout <2
    % this is the easy case, do Spc*(W\Scp)
    W = (Gcc-Scc);
    DATA = Spp + MultiMatrix.run(@(A,B,C) A*(B\C),Spc,W,Scp);
elseif nargout == 2
    % Compute first the inverse of (Gcc-Scc) to be reused later
    W = (Gcc-Scc)\eye(size(Scc,1),size(Scc,2));
    % calculate DATA = Spp+Spc*W\Scp
    % SpcW = Spc*W;
    % DATA = SpcW * Scp;
    SpcW = Spc * W;
    DATA = Spp + SpcW*Scp;
    % Add zeros
    nparam = size(dScp,3)/size(Scp,3);
    % calculate WScp = W*Scp
    WScp = W * Scp;
    % repmat for each parameter
    WScp = repmat(WScp,1,1,nparam);
    SpcW = repmat(SpcW,1,1,nparam);
    % calculate DER = dSpc*W*Scp + Spc*W*(dScc*W*Scp + dScp);
    DER = MultiMatrix.run(@(SpcW,WScp,dSpc,dScc,dScp) (dSpc + SpcW*dScc)*WScp + SpcW*dScp,SpcW,WScp,dSpc,dScc,dScp);
    DER = dSpp + DER;
end


%% cast it to a MultiPort
% create a new object if the frequency is given
if ~isempty(args.frequency)
    if P == 2
        RES = TwoPort(DATA,'Freq',args.frequency,unmatched);
    else
        RES = MultiPort(DATA,'Freq',args.frequency,unmatched);
    end
    
else
% find a good candidate MultiPort in the componentStruct to overwrite
F = structfun(@(x) size(x,3),componentStruct);
[~,ind]=max(F);
fields = fieldnames(componentStruct);
RES = MultiPort(componentStruct.(fields{ind}));
% overwrite the data in the multiport
RES.Data = DATA;
% if the amount of ports is two, cast to a TwoPort
if P==2
    RES = TwoPort(RES);
end
end

end


function [loc,occur] = findLocations(cell1Num,cell2Num)
% Return the location of elements of cell1 in cell2 ant the number of occurrences
cell1Num = reshape(cell1Num,1,[]);
cell2Num = reshape(cell2Num,[],1);

%% Find component location
ind = cell2Num==cell1Num;
[loc,~] = find(cell1Num==cell2Num);
loc = reshape(loc,1,[]);
%% Number of occurences
if nargout ==2
    occur = sum(ind,1);
end
end

function [ num ] = hash( str )
%HASH transforms a string into a unique number
% I assume that all the characters are from the range char(48:122) which
% corresponds to:
%   0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz
% The strings are first converted to values between 1 and 75
% then they are multiplied by 75^N and summed together
num = sum((double(str)-47).*75.^(0:length(str)-1));
end

% @generateFunctionHelp

% @Tagline connects several MultiPorts together according to a provided netlist

% @Example cascade of two filters
% @Example
% @Example    componentStruct =
% @Example        Filter1: [2x2x100 TwoPort]
% @Example        Filter2: [2x2x100 TwoPort]
% @Example    netlist =
% @Example        {
% @Example            'Term a';
% @Example            'Filter1 a b';
% @Example            'Filter2 b c';
% @Example            'Term c';
% @Example         }
% @Example    res = interconnect(componentStruct,netlist)     

% @Outputs{1}.Name result
% @Outputs{1}.Description{1} MultiPort which contains the description of the total circuit.
% @Outputs{1}.Type MultiPort or TwoPort

% @Outputs{2}.Name derivative
% @Outputs{2}.Description{1} MultiMatrix which contains the derivative of the obtained S-parameters to the elements in the circuit
% @Outputs{2}.Type MultiMatrix
