clear variables
close all
clc

%% netlist which uses only components

% make a small netlist with two resistors
netlist = { {'SeriesL a b |','L' 50},...
            {'SeriesL b c |','L',100}};
% and call the interconnect function
RES1 = interconnect(struct(),netlist,'frequency',1:3,'useterminations',false,'layout',false);

netlist = { {'SeriesL a b |','L' 150}};
% and call the interconnect function
RES2 = interconnect(struct(),netlist,'frequency',1:3,'useterminations',false,'layout',false);

%% netlist which uses a mix of components and MultiPorts
% figure
% generate a random system
componentStruct.AMP = TwoPort(rand(2,2,100),'Freq',reshape(linspace(0.1,1,100),1,1,[]));
% connect two resistors to the AMP
netlist = { ...
            {'SeriesR a b' '|' 'R' 50},...
            'AMP b c',...
            'AMP c gnd',...
            {'ShuntR c d |','R' 100}...
          };
% and call the interconnect function
RES = interconnect(componentStruct,netlist,'useterminations',false,'layout',false);

%% netlist which uses Terminations
% use terminations to indicate the ports
% figure
netlist = { 'Term a',...
            {'SeriesL a b' '|' 'L' 50e-9},...
            'AMP b c',...
            {'ShuntC b c |' 'C' 100e-12},...
            {'SeriesR c d |' 'R' 100},...
            'Term d'};
% and call the interconnect function
RES = interconnect(componentStruct,netlist,'layout',false);



