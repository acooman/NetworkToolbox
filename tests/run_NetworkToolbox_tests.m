function run_NetworkToolbox_tests(varargin)

p = inputParser();
p.PartialMatching=true;
p.addParameter('run_FRM_tests',false,@islogical);
p.addParameter('run_MultiPort_tests',false,@islogical);
p.addParameter('run_plot_tests',false,@islogical);
p.addParameter('run_operator_tests',false,@islogical);
p.addParameter('run_interconnect_tests',false,@islogical);
p.addParameter('run_All',false,@islogical);
p.parse(varargin{:})
args = p.Results;
% if run_All is true, set all the fields to true
if args.run_All
    args = structfun(@(x) true,args,'UniformOutput',false);
end

% if they are all false, display some info
if all(structfun(@(x) ~x,args))
    disp('You selected no tests to run. You can set the following options to true to run the test:')
    disp(fieldnames(args))
end

% go to the right folder
fi = mfilename('fullpath');
path = fileparts([fi,'.m']);
cd(path)
cd ..

import matlab.unittest.TestSuite
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin

%% test the FRM class
if args.run_FRM_tests
    % add the tested folders to the list of folders to profile
    codeFolders = {fullfile(pwd,'source','@FRM')};
    
    % generate the test suite
    suite = TestSuite.fromFile(fullfile('tests','testFRM.m'));
    
    % remove the unwanted tests from the suite
    if ~args.run_plot_tests
        suite = suite(cellfun(@(x) isempty(x),regexp({suite.Name},'[pP]lot')));
    end
    if ~args.run_operator_tests
        suite = suite(cellfun(@(x) isempty(x),regexp({suite.Name},'[oO]perator')));
    end
    
    % generate a runner with a code coverage plugin
    runner = TestRunner.withTextOutput;
    runner.addPlugin(CodeCoveragePlugin.forFolder(codeFolders));
    
    % run the test suite
    result = runner.run(suite);
end


%% test the MultiPort and TwoPort classes
if args.run_MultiPort_tests
    codeFolders = {fullfile(pwd,'source','@TwoPort') fullfile(pwd,'source','@MultiPort')};
    suite = TestSuite.fromFile(fullfile('tests','testMultiPort.m'));
    runner = TestRunner.withTextOutput;
    runner.addPlugin(CodeCoveragePlugin.forFolder(codeFolders));
    result = runner.run(suite);
end


% test the interconnect function
if args.run_interconnect_tests
    codeFolders = {fullfile(pwd,'source','interConnect')};
    suite = TestSuite.fromFile(fullfile('tests','testInterconnect.m'));
    runner = TestRunner.withTextOutput;
    runner.addPlugin(CodeCoveragePlugin.forFolder(codeFolders));
    result = runner.run(suite);
end

end

