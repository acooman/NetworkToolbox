classdef testFRM < matlab.unittest.TestCase
    %TESTFRM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (TestParameter)
        operator = struct('MTIMES',@mtimes,'PLUS',@plus,'MLDIVIDE',@mldivide,'MRDIVIDE',@mrdivide,'MINUS',@minus,'TIMES',@times,'RDIVIDE',@rdivide)
        domain = {'PLANE','DISC'};
        normalisation = {@LowpassNormalisation,@BandpassNormalisation};
        F = struct('small', 1,'medium', 10, 'large', 100);
        F2 = struct('equal',Inf,'single',1);
        N = struct('small', 1,'medium', 2, 'large', 3);
        M = struct('small', 1,'medium', 2, 'large', 3);
        PlotTypes = {[],'s','a','w','d','D','R','I','N'};
        resampleMethod = {'SPLINE','LINEAR','RATIONAL'};
    end
    
    methods (Test)
        %% CONSTRUCTOR
        function test_constructor_PLANE(testCase,N,M,F,normalisation)
            % check the different cases in which the constructor is used
            % none of these should throw an error
            data = rand(N,M,F);
            A = FRM(data,'Freq',1:F);
            norm = normalisation(1:F);
            A = FRM(data,'Freq',1:F,'domain','PLANE','Normalisation',norm);
            % check the disp function
            disp(A);
        end
        function test_constructor_DISC(testCase,N,M,F,normalisation)
            % check the different cases in which the constructor is used
            % none of these should throw an error
            data = rand(N,M,F);
            theta = linspace(pi,pi+pi/2,F);
            A = FRM(data,'Theta',theta,'Domain','DISC');
            norm = normalisation(-1,1);
            A = FRM(data,'Theta',linspace(pi,pi+pi/2,F),'domain','DISC','Normalisation',norm);
        end
        
        %% GETTERS and SETTERS
        function test_get_Freq(testCase,F,normalisation)
            % test whether we get the same Freq axis back as we originally apply
            Freq = 1:F;
            norm = normalisation(Freq);
            A = FRM(rand(1,1,F),'Freq',Freq,'Normalisation',norm);
            RES = A.Freq;
            testCase.verifyEqual(RES , permute(Freq(:),[3 2 1]) ,'AbsTol',1e-14);
        end
        function test_get_Theta(testCase,F,normalisation)
            Theta = linspace(0,1,F);
            norm = normalisation(-1,1);
            A = FRM(rand(1,1,F),'Theta',Theta,'Domain','DISC','Normalisation',norm);
            RES = A.Theta;
            testCase.verifyEqual(RES , permute(Theta(:),[3 2 1]) ,'AbsTol',1e-14);
        end
        function test_getset_Freq_normalised(testCase,F)
            A = FRM(rand(1,1,F),'Freq',linspace(1,100,F));
            A.Freq_normalised = linspace(-1,1,F);
            testCase.verifyEqual(A.Freq_normalised,reshape(linspace(-1,1,F),[1,1,F]));
        end
        function test_getset_Normalisation(testCase,F)
            Freq = logspace(1,2,F);
            A = FRM(rand(1,1,F),'Freq',Freq,'Domain','PLANE');
            % create a new normalisation
            norm = LowpassNormalisation(Freq);
            % set the new normalisation
            A.Normalisation = norm;
            % test whether the frequency axis stayed the same
            testCase.verifyEqual(reshape(Freq,[1,1,F]),A.Freq,'AbsTol',1e-14);
            % also test the getter
            norm = A.Normalisation;
        end
        %% OPERATORS
        function test_operators(testCase,operator,N,F,F2)
            % this test verifies whether the product of a MultiMatrix with
            % a MultiMatrix works.
            freq1 = linspace(0,100,F);
            if isinf(F2)
                F2 = F;
            end
            freq2 = linspace(0,100,F2);
            IN1 = rand(N,N,F)+1i*rand(N,N,F);
            if isinf(F2)
                IN2 = rand(N,N,F)+1i*rand(N,N,F);
                % calculate the expected value with a simple for-loop
                for ff=1:F
                    expected(:,:,ff) = operator(IN1(:,:,ff),IN2(:,:,ff));
                end
            else
                IN2 = rand(N,N)+1i*rand(N,N);
                % calculate the expected value with a simple for-loop
                for ff=1:F
                    expected(:,:,ff) = operator(IN1(:,:,ff),IN2(:,:));
                end
            end
            % use the operator defined in the class
            RES = operator(FRM(IN1,'Freq',freq1),FRM(IN2,'Freq',freq2));
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES.Data) , expected,'AbsTol',1e-10)
            RES = operator(FRM(IN1,'Freq',freq1),IN2);
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES.Data) , expected,'AbsTol',1e-10)
            RES = operator(IN1,FRM(IN2,'Freq',freq2));
            % compare the obtained result to the expected result
            testCase.verifyEqual(double(RES.Data) , expected,'AbsTol',1e-10)
            
        end
        function test_uminus(testCase,N,M,F)
            IN1 = rand(N,M,F)+1i*rand(N,M,F);
            RES = uminus(FRM(IN1,'Freq',linspace(0,100,F)));
            expected = -IN1;
            testCase.verifyEqual(double(RES.Data) , expected,'AbsTol',1e-10)
        end
        
        %% test the subsref function
        function test_subsref(testCase,N,M,F)
            obj = FRM(rand(N,M,F)+1i*rand(N,M,F),'Freq',linspace(0,100,F));
            % try the accessing with round brackets
            % this should return an object
            assert(isa(obj(1,end,:),'FRM'));
            assert(isa(obj(1,end),'FRM'));
            assert(isa(obj(1),'FRM'));
            % try to select a certain frequency range
            test = obj(1,end,1);
            assert(isa(test,'FRM'));
            assert(size(test.Freq,3)==1);
        end
        %% tests for the size functions
%         function test_size(testCase,N,M,F)
%             IN = FRM(rand(N,M,F));
%             testCase.verifyEqual(size(IN) , [N M F]);
%             testCase.verifyEqual(size(IN,1) , N);
%             testCase.verifyEqual(size(IN,2) ,M);
%             testCase.verifyEqual(size(IN,3) ,F);
%             [A,B,C] = size(IN);
%             testCase.verifyEqual(A , N);
%             testCase.verifyEqual(B , M);
%             testCase.verifyEqual(C , F);
%         end
        function test_length(testCase,N,M,F)
            IN = FRM(rand(N,M,F),'Freq',linspace(0,100,F));
            testCase.verifyEqual(length(IN) , F);
        end
        %% TRANSFORM function
        function test_transform_size(testCase,N,M,F,domain,normalisation)
            Freq = 1:F;
            % generate the normalisation
            norm = normalisation(Freq);
            % generate the original FRM on the PLANE
            A = FRM(rand(N,M,F),'Freq',Freq,'Normalisation',norm,'Domain','PLANE');
            % transform the FRM to the provided domain
            B = A;
            B.Domain = 'DISC';
            % transform back to the PLANE
            B.Domain = 'PLANE';
            % check the stuff
            testCase.verifyEqual(double(A.Data),double(B.Data),'AbsTol',1e-12);
            testCase.verifyEqual(A.Freq,B.Freq,'AbsTol',1e-12);
        end
%         function test_transform_result(testCase)
%             a = rand();
%             b = rand();
%             c = rand();
%             d = rand();
%             % get a frequency axis. Ensure that it is a normalised axis
%             % already, otherwise the normalisation will mess up the test
%             f= zeros(1,1,100);
%             f(1,1,:) = linspace(0,1,100);
%             % s is the laplace variable
%             s = 2*pi*1i*f;
%             % make an FRM with the rational function (as+b)/(cs+d)
%             Z = FRM((a*s+b)./(c*s+d),'Freq',f,'Domain','PLANE','NormType','LOWPASS');
%             % transform Z to the unit disc
%             Z.Domain = 'DISC';
%             % get the alpha which is used
%             alpha = Z.alpha;
%             % I also have the analytical solution 
%             Zdisctest = @(z) 2*sqrt(pi*alpha)*((a*alpha-b)*z+(a*alpha+b))./((c*alpha-d)*z.^2+2*d*z-(c*alpha+d));
%             % compare both results
%             testCase.verifyEqual(double(Z.Data),Zdisctest(exp(1i*Z.Theta)),'AbsTol',1e-13);
%         end
        
        %% PLOT function
        function test_plot_FRM(testCase,N,M,F,PlotTypes,normalisation)
            % I just check that the plot function doesn't give any errors
            Freq = linspace(0,1,F);
            % generate the normalisation
            norm = normalisation(Freq);
            % generate the original FRM on the PLANE
            A = FRM(rand(N,M,F),'Freq',Freq,'Normalisation',norm,'Domain','PLANE');
            if ~isempty(PlotTypes)
                h = plot(A,'Config',PlotTypes);
            else
                h = plot(A);
            end
            % go to the disc
            A.Domain = 'DISC';
            % and plot on the disc
            if ~isempty(PlotTypes)
                h = plot(A,'Config',PlotTypes);
            else
                h = plot(A);
            end
            % go to the disc
            A.Domain = 'DISC';
            % and plot on the disc
            if ~isempty(PlotTypes)
                h = plot(A,'Config',PlotTypes);
            else
                h = plot(A);
            end
        end
        
        %% RESAMPLE function
        function test_resample_plane(testCase,N,M,F,normalisation,resampleMethod)
            % I just check that the plot function doesn't give any errors
            Freq = linspace(0,1,F);
            % generate the normalisation
            norm = normalisation(Freq);
            % generate the original FRM on the PLANE
            A = FRM(rand(N,M,F),'Freq',Freq,'Normalisation',norm,'Domain','PLANE');
            % resample A to B
            B = resample(A,linspace(0.1,0.9,F),'Method',resampleMethod);
        end
        function test_resample_disc(testCase,N,M,F,resampleMethod)
            if F>=3
                % I just check that the plot function doesn't give any errors
                Theta = linspace(0,2*pi,F);
                % generate the original FRM on the PLANE
                A = FRM(rand(N,M,F),'Theta',Theta,'Domain','DISC');
                % resample A to B
                try
                    B = resample(A,linspace(0,2*pi,2*F),'Method',resampleMethod);
                catch
                    keyboard
                end
            end
        end
    end
end
