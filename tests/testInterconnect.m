classdef testInterconnect < matlab.unittest.TestCase
    %TESTFRM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (TestParameter)
        F = struct('small', 1,'medium', 10, 'large', 100);
        Sparse = struct('yes',true,'no',false);
        Terminations = struct('yes',true,'no',false);
        Checks = struct('yes',true,'no',false);
    end
    
    methods (Test)

        function test_getset_NormType(testCase,F,Sparse,Terminations)
            % generate some random components
            comps.A = TwoPort(rand(2,2,F),'Freq',linspace(0,1,F));
            comps.B = TwoPort(rand(2,2,F),'Freq',linspace(0,1,F));
            % chain them together
            netlist={};
            netlist{end+1}='A 1 2';
            netlist{end+1}='B 2 3';
            if Terminations
                netlist{end+1}='Term 1';
                netlist{end+1}='Term 3';
            end
            % interconnect the two together
            RES = interconnect(comps,netlist,'useSparse',Sparse,'useTerminations',Terminations);
            % also chain the two to compare against
            TEST = comps.B.T.Data*comps.A.T.Data;
            % check the result
            testCase.verifyEqual(double(RES.T.Data),double(TEST),'AbsTol',1e-12);
        end
        
        
    end
end
