classdef testMultiPort < matlab.unittest.TestCase
    
    properties (TestParameter)
        % Representations
        Multiport_Representations = struct(...
            'Sparams','S',...
            'Zparams','Z',...
            'Yparams','Y');
        Twoport_Representations = struct(...
            'Sparams','S',...
            'Zparams','Z',...
            'Yparams','Y',...
            'Gparams','G',...
            'Hparams','H',...
            'Aparams','A',...
            'Bparams','B',...
            'Tparams','T');
        % F is the amount of frequencies
        F = struct('small', 1,'medium', 10, 'large', 100);
        % P is the number of ports
        P = struct('small', 1,'medium', 2, 'large', 10);
        ReadWriteType = struct(...
            'Sparams','S',...
            'Zparams','Z',...
            'Yparams','Y',...
            'Gparams','G',...
            'Hparams','H',...
            'Tparams','T');
    end
    
    methods (Test)
        function test_Multiport_constructor(testCase,Multiport_Representations,F,P)
            data = rand(P,P,F)+1i*rand(P,P,F);
            freq = linspace(0,100,F);
            TestObject = MultiPort(data,'Type',Multiport_Representations,'Freq',freq);
            % get the original type back
            dataTest = TestObject.(Multiport_Representations);
            % verify that the object coming out is an FRM
            testCase.assertClass(dataTest,'FRM');
            % verify whether the data that went in is equal to the data that came out
            testCase.assertEqual(data,double(dataTest.Data),'absTol',1e-9);
        end
        
        function test_TwoPort_constructor(testCase,Twoport_Representations,F)
            data = rand(2,2,F)+1i*rand(2,2,F);
            freq = linspace(0,100,F);
            S = TwoPort(data,'Type',Twoport_Representations,'Freq',freq);
            % get the original type back
            dataTest = S.(Twoport_Representations);
            % verify that the object coming out is an FRM
            testCase.assertClass(dataTest,'FRM');
            % verify whether the data that went in is equal to the data that came out
            testCase.assertEqual(data,double(dataTest.Data),'absTol',1e-9);
        end
        
        function test_indexing(testCase,F,P)
            % test whether the port suppression works
            MP = MultiPort(rand(P,P,F),'Freq',linspace(0,100,F));
            % just get the first port
            MP(1);
            % check whether the casting to twoport does its job
            if P>2
                testCase.assertClass(MP(1:2,1:2,:),'TwoPort');
            end
        end
        
        function test_getset_Ref(testCase,F,P)
            data = rand(P,P,F);
            MP = MultiPort(data,'ZRef',50,'Freq',linspace(0,100,F));
            % go to 100 Ohm and back
            MP.Ref = 100;
            MP.Ref = 50;
            % check that the end result has not changed
            testCase.verifyEqual(double(MP.Data),data,'AbsTol',1e-14);
            % also test the getter
            t = MP.Ref;
        end
        
        function test_readwrite(testCase,F,P,ReadWriteType)
            data = rand(P,P,F)+1i*rand(P,P,F);
            freq = linspace(0,100,F);
            if P~=2
                if ~ismember(ReadWriteType,{'S','Y','Z'})
                    return
                else
                    T = MultiPort(data,'Freq',freq);
                end
            else
                T = TwoPort(data,'Freq',freq);
            end
            % write the multiport to a file
            write(T,'Name',fullfile(cd,'temp'),'Type',ReadWriteType,'version',2)
            % read the generated file back into a second object
            T2 = MultiPort.read('temp');
            % delete the generated file
            delete('temp*')
            % compare the data and the frequency axes in both objecs
            testCase.verifyEqual(double(T.Data),double(T2.Data),'Abstol',1e-3);
            testCase.verifyEqual(double(T.Freq),double(T2.Freq),'Abstol',1e-3);
        end
    end
end

